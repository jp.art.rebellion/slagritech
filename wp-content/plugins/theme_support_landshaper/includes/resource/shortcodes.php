<?php
$bunch_sc = array();
$bunch_sc['bunch_welcome_our_company'] = array(
			"name" => __("Welcome Our Company", BUNCH_NAME),
			"base" => "bunch_welcome_our_company",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Welcome Our Company', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Sub Title', BUNCH_NAME ),
						   "param_name" => "sub_title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text1",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text2",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Link', BUNCH_NAME ),
						   "param_name" => "btn_link",
						   "description" => __('Enter The Button Link to Show.', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Text', BUNCH_NAME ),
						   "param_name" => "btn_text",
						   "description" => __('Enter The Button Text to Show.', BUNCH_NAME )
						),
						// params group
			            array(
			                'type' => 'param_group',
			                'value' => '',
			                'param_name' => 'services',
							'group' => esc_html__('Our Services', BUNCH_NAME),
			                'params' => array(
										array(
										   "type" => "attach_image",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __("Image", BUNCH_NAME),
										   "param_name" => "image",
										   "description" => __("Enter the Section Image to show.", BUNCH_NAME)
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Button Link', BUNCH_NAME ),
											'param_name' => 'btn_link',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Button Text', BUNCH_NAME ),
											'param_name' => 'btn_text',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Title', BUNCH_NAME ),
											'param_name' => 'title',
										),
										array(
											'type' => 'textarea',
											'value' => '',
											'heading' => esc_html__('Text', BUNCH_NAME ),
											'param_name' => 'text',
										), 
									)
								),
							),
						);
$bunch_sc['bunch_call_to_action']	=	array(
					"name" => __("Call To Action", BUNCH_NAME),
					"base" => "bunch_call_to_action",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Call To Action.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Background Image", BUNCH_NAME),
								   "param_name" => "bg_img",
								   "description" => __("Enter the Section Image to show.", BUNCH_NAME)
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
							)
						);
$bunch_sc['bunch_latest_projects']	=	array(
					"name" => __("Latest Projects", BUNCH_NAME),
					"base" => "bunch_latest_projects",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Latest Projects.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Background Image", BUNCH_NAME),
								   "param_name" => "bg_image",
								   "description" => __("Enter the Section Image to show.", BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title1",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Color Title', BUNCH_NAME ),
								   "param_name" => "title2",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Projects to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Excluded Categories ID', BUNCH_NAME ),
								   "param_name" => "exclude_cats",
								   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
									"type" => "checkbox",
									"holder" => "div",
									"class" => "",
									"heading" => __('Style Two', BUNCH_NAME ),
									"param_name" => "style_two",
									'value' => array(__('Style Two for change the container', BUNCH_NAME )=>true),
									"description" => __('Choose whether you want to show full Container.', 'SH_NAME' )
								),
							)
						);
$bunch_sc['bunch_why_choose_us']	=	array(
					"name" => __("Why Choose Us", BUNCH_NAME),
					"base" => "bunch_why_choose_us",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Why Choose Us.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link",
								   "description" => __('Enter The Button Link to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Text', BUNCH_NAME ),
								   "param_name" => "btn_text",
								   "description" => __('Enter The Button Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Limit Of Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Slides to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_funfacts'] = array(
			"name" => __("Funfacts", BUNCH_NAME),
			"base" => "bunch_funfacts",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Welcome Decorator', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', BUNCH_NAME ),
						   "param_name" => "image",
						   "description" => __('Enter section Background Image To Show', BUNCH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME )
						),
						// params group
			            array(
			                'type' => 'param_group',
			                'value' => '',
			                'param_name' => 'funfacts',
							'group' => esc_html__('Funfacts', BUNCH_NAME),
			                'params' => array(
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Counter', BUNCH_NAME ),
											'param_name' => 'counter',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Symbols', BUNCH_NAME ),
											'param_name' => 'symbol',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Title', BUNCH_NAME ),
											'param_name' => 'title',
										), 
										array(
										   "type" => "dropdown",
										   'value' => '',
										   "heading" => __("Icon", BUNCH_NAME),
										   "param_name" => "icon",
										   "value" => (array)vp_get_fontawesome_icons(),
										   "description" => __("Choose Icon for Process", BUNCH_NAME)
										),
									)
								),
							),
						);
$bunch_sc['bunch_latest_news']	=	array(
					"name" => __("Latest News", BUNCH_NAME),
					"base" => "bunch_latest_news",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Latest News.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link",
								   "description" => __('Enter The Button Link to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Text', BUNCH_NAME ),
								   "param_name" => "btn_text",
								   "description" => __('Enter The Button Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_testimonial_slider']	=	array(
					"name" => __("Testimonial Slider", BUNCH_NAME),
					"base" => "bunch_testimonial_slider",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Testimonial Slider.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Background Image', BUNCH_NAME ),
								   "param_name" => "bg_img",
								   "description" => __('Enter section Background Image To Show', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title1",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title2",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Limit Of Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_request_an_estimate']	=	array(
					"name" => __("Request an estimate", BUNCH_NAME),
					"base" => "bunch_request_an_estimate",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Request an estimate.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Lattitude', BUNCH_NAME ),
								   "param_name" => "lat",
								   "description" => __('Enter Lattitude for map', BUNCH_NAME ),
								   "default" => '44.953021',
								   'group' => esc_html__('Google Map', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Longitude', BUNCH_NAME ),
								   "param_name" => "long",
								   "description" => __('Enter Longitude for map', BUNCH_NAME ),
								   "default" => '-92.995215',
								   'group' => esc_html__('Google Map', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Mark Title', BUNCH_NAME ),
								   "param_name" => "mark_title",
								   "description" => __('Enter Mark Title for map', BUNCH_NAME ),
								   "default" => 'Brooklyn, New York, United Kingdom',
								   'group' => esc_html__('Google Map', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
								array(
								   "type" => "textarea_raw_html",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Contact Form7 Shortcode', BUNCH_NAME ),
								   "param_name" => "contact_form",
								   "description" => __('Enter Contact Form7 Shortcode', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "content",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Office Hours', BUNCH_NAME ),
								   "param_name" => "office_hours",
								   "description" => __('Enter The Office Hours to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
							)
						);
$bunch_sc['bunch_our_clients']	=	array(
					"name" => __("Our Clients", BUNCH_NAME),
					"base" => "bunch_our_clients",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Our Clients.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Describtion', BUNCH_NAME ),
								   "param_name" => "describtion",
								   "description" => __('Enter The Description for Own Help.', BUNCH_NAME )
								),
							)
						);
$bunch_sc['bunch_our_services']	=	array(
					"name" => __("Our Services", BUNCH_NAME),
					"base" => "bunch_our_services",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Our Services.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Text Limit to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_request_an_estimate_2']	=	array(
					"name" => __("Request an estimate 2", BUNCH_NAME),
					"base" => "bunch_request_an_estimate_2",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Request an estimate 2.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Image', BUNCH_NAME ),
								   "param_name" => "image",
								   "description" => __('Enter section Image To Show', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
								array(
								   "type" => "textarea_raw_html",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Contact Form7 Shortcode', BUNCH_NAME ),
								   "param_name" => "contact_form",
								   "description" => __('Enter Contact Form7 Shortcode', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "content",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Office Hours', BUNCH_NAME ),
								   "param_name" => "office_hours",
								   "description" => __('Enter The Office Hours to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
							)
						);
$bunch_sc['bunch_product_slider']	=	array(
					"name" => __("Product Slider", BUNCH_NAME),
					"base" => "bunch_product_slider",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Product Slider.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title1",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'product_cat', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);						
$bunch_sc['bunch_home_3_slider']	=	array(
					"name" => __("Home 3 Slider", BUNCH_NAME),
					"base" => "bunch_home_3_slider",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Home 3 Slider.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Slider Image", BUNCH_NAME),
								   "param_name" => "img1",
								   "description" => __("Enter the Slider Image to show.", BUNCH_NAME)
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Slider Image", BUNCH_NAME),
								   "param_name" => "img2",
								   "description" => __("Enter the Slider Image to show.", BUNCH_NAME)
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Slider Image", BUNCH_NAME),
								   "param_name" => "img3",
								   "description" => __("Enter the Slider Image to show.", BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title One', BUNCH_NAME ),
								   "param_name" => "title1",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title Two', BUNCH_NAME ),
								   "param_name" => "title2",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text One', BUNCH_NAME ),
								   "param_name" => "text1",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Two', BUNCH_NAME ),
								   "param_name" => "text2",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link One', BUNCH_NAME ),
								   "param_name" => "btn_link1",
								   "description" => __('Enter The Button Link to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Text One', BUNCH_NAME ),
								   "param_name" => "btn_text1",
								   "description" => __('Enter The Button Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link Two', BUNCH_NAME ),
								   "param_name" => "btn_link2",
								   "description" => __('Enter The Button Link to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Text Two', BUNCH_NAME ),
								   "param_name" => "btn_text2",
								   "description" => __('Enter The Button Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title3",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
								array(
								   "type" => "textarea_raw_html",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Contact Form7 Shortcode', BUNCH_NAME ),
								   "param_name" => "slider_form",
								   "description" => __('Enter Contact Form7 Shortcode', BUNCH_NAME ),
								   'group' => esc_html__('Request An Estimate', BUNCH_NAME),
								),
							)
						);
$bunch_sc['bunch_why_choosing']	=	array(
					"name" => __("Why Choosing", BUNCH_NAME),
					"base" => "bunch_why_choosing",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Why Choosing.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Text Limit to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_testimonial_feedback'] = array(
			"name" => __("Testimonial Feedback", BUNCH_NAME),
			"base" => "bunch_testimonial_feedback",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Testimonial Feedback', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Sub Title', BUNCH_NAME ),
						   "param_name" => "sub_title1",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Our Best Services', BUNCH_NAME),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title1",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Our Best Services', BUNCH_NAME),
						),
						// params group
			            array(
			                'type' => 'param_group',
			                'value' => '',
			                'param_name' => 'accordion',
							'group' => esc_html__('Our Best Services', BUNCH_NAME),
			                'params' => array(
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Title', BUNCH_NAME ),
											'param_name' => 'title',
										),
										array(
											'type' => 'textarea',
											'value' => '',
											'heading' => esc_html__('Text', BUNCH_NAME ),
											'param_name' => 'text',
										), 
									)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title2",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title2",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Limit Of Text to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								
							),
						);
$bunch_sc['bunch_our_team']	=	array(
					"name" => __("Our Team", BUNCH_NAME),
					"base" => "bunch_our_team",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Our Team.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "content",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Limit Of Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'team_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_our_estimation']	=	array(
					"name" => __("Our Estimation", BUNCH_NAME),
					"base" => "bunch_our_estimation",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Our Estimation.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textarea_raw_html",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Contact Form7 Shortcode', BUNCH_NAME ),
								   "param_name" => "our_estimation",
								   "description" => __('Enter Contact Form7 Shortcode', BUNCH_NAME )
								),
							)
						);
$bunch_sc['bunch_video_section']	=	array(
					"name" => __("Video Section", BUNCH_NAME),
					"base" => "bunch_video_section",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Video Section.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text One', BUNCH_NAME ),
								   "param_name" => "text1",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Two', BUNCH_NAME ),
								   "param_name" => "text2",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "attach_images",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Signature Images", BUNCH_NAME),
								   "param_name" => "sign_img",
								   "description" => __("Enter the Signature Image to show.", BUNCH_NAME)
								),
								array(
								   "type" => "attach_images",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Video Images", BUNCH_NAME),
								   "param_name" => "video_img",
								   "description" => __("Enter the Section Image to show.", BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Video Link', BUNCH_NAME ),
								   "param_name" => "video_link",
								   "description" => __('Enter The Video Link to Show.', BUNCH_NAME )
								),
							)
						);
$bunch_sc['bunch_services_3_col']	=	array(
					"name" => __("Services 3 column", BUNCH_NAME),
					"base" => "bunch_services_3_col",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Services 3 column.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Text Limit to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_pricing_section'] = array(
			"name" => __("Pricing Section", BUNCH_NAME),
			"base" => "bunch_pricing_section",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Pricing Section', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME )
						),
						// params group
			            array(
			                'type' => 'param_group',
			                'value' => '',
			                'param_name' => 'pricing_table',
							'group' => esc_html__('Pricing Table', BUNCH_NAME),
			                'params' => array(
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Currency', BUNCH_NAME ),
											'param_name' => 'currency',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Price', BUNCH_NAME ),
											'param_name' => 'price',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Title', BUNCH_NAME ),
											'param_name' => 'title',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Duration', BUNCH_NAME ),
											'param_name' => 'duration',
										),
										array(
										   "type" => "textarea",
										   "holder" => "div",
										   "class" => "",
										   "heading" => __("Features Text", BUNCH_NAME),
										   "param_name" => "feature_str",
										   "description" => __("Enter the Section Features to show.", BUNCH_NAME)
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Button Link', BUNCH_NAME ),
											'param_name' => 'btn_link',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Button Text', BUNCH_NAME ),
											'param_name' => 'btn_text',
										),
									)
								),
							),
						);
$bunch_sc['bunch_about_us']	=	array(
					"name" => __("About Us", BUNCH_NAME),
					"base" => "bunch_about_us",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show About Us.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text First', BUNCH_NAME ),
								   "param_name" => "text1",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Second', BUNCH_NAME ),
								   "param_name" => "text2",
								   "description" => __('Enter The Description to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link",
								   "description" => __('Enter The Button Link to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Text', BUNCH_NAME ),
								   "param_name" => "btn_text",
								   "description" => __('Enter The Button Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Image", BUNCH_NAME),
								   "param_name" => "bg_img",
								   "description" => __("Enter the Section Image to show.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_our_history'] = array(
			"name" => __("Our History", BUNCH_NAME),
			"base" => "bunch_our_history",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Our History', BUNCH_NAME),
			"params" => array(
						// params group
			            array(
			                'type' => 'param_group',
			                'value' => '',
			                'param_name' => 'our_history',
							'group' => esc_html__('Our History', BUNCH_NAME),
			                'params' => array(
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Years', BUNCH_NAME ),
											'param_name' => 'years',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Title', BUNCH_NAME ),
											'param_name' => 'title',
										),
										array(
										   "type" => "attach_image",
										   'value' => '',
										   "heading" => __("Image", BUNCH_NAME),
										   "param_name" => "img",
										),
										array(
											'type' => 'textarea',
											'value' => '',
											'heading' => esc_html__('Text', BUNCH_NAME ),
											'param_name' => 'text',
										),
									)
								),
							),
						);
$bunch_sc['bunch_testimonial_3_col']	=	array(
					"name" => __("Testimonial 3 Col", BUNCH_NAME),
					"base" => "bunch_testimonial_3_col",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Testimonial 3 Col.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Limit Of Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_faqs_style_one']	=	array(
					"name" => __("Faqs Style One", BUNCH_NAME),
					"base" => "bunch_faqs_style_one",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Faqs Style One.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Limit Of Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'faqs_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_faqs_style_two'] = array(
			"name" => __("Faqs Style Two", BUNCH_NAME),
			"base" => "bunch_faqs_style_two",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Faqs Style Two', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Accordion Style One', BUNCH_NAME)
						),
						// params group
			            array(
			                'type' => 'param_group',
			                'value' => '',
			                'param_name' => 'accordion_one',
							'group' => esc_html__('Accordion Style One', BUNCH_NAME),
			                'params' => array(
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Title', BUNCH_NAME ),
											'param_name' => 'title',
										),
										array(
											'type' => 'textarea',
											'value' => '',
											'heading' => esc_html__('Text One', BUNCH_NAME ),
											'param_name' => 'text1',
										),
										array(
											'type' => 'textarea',
											'value' => '',
											'heading' => esc_html__('Text Two', BUNCH_NAME ),
											'param_name' => 'text2',
										),
									)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title1",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Accordion Style Two', BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Color Title ', BUNCH_NAME ),
								   "param_name" => "title2",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Accordion Style Two', BUNCH_NAME)
								),
								// params group
								array(
									'type' => 'param_group',
									'value' => '',
									'param_name' => 'accordion_two',
									'group' => esc_html__('Accordion Style Two', BUNCH_NAME),
									'params' => array(
												array(
													'type' => 'textfield',
													'value' => '',
													'heading' => esc_html__('Title', BUNCH_NAME ),
													'param_name' => 'title1',
												),
												array(
													'type' => 'textarea',
													'value' => '',
													'heading' => esc_html__('Text One', BUNCH_NAME ),
													'param_name' => 'text3',
												),
												array(
													'type' => 'textarea',
													'value' => '',
													'heading' => esc_html__('Text Two', BUNCH_NAME ),
													'param_name' => 'text4',
												),
											)
										),
									),
								);
$bunch_sc['bunch_appoinment_form']	=	array(
					"name" => __("Appointment Form", BUNCH_NAME),
					"base" => "bunch_appoinment_form",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Appointment Form', BUNCH_NAME),
					"params" => array(
					   	
					 	array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter section Title', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Sub Title', BUNCH_NAME ),
						   "param_name" => "title1",
						   "description" => __('Enter section Sub Title', BUNCH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Receiver Email', BUNCH_NAME ),
						   "param_name" => "email",
						   "description" => __('Enter Receiver Email', BUNCH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Services', BUNCH_NAME ),
						   "param_name" => "services_needed_str",
						   "description" => __('Enter Services 1 per line.', BUNCH_NAME )
						),
					)
				);								
$bunch_sc['bunch_electric_mover'] = array(
				 "name" => __("Electric Mover", BUNCH_NAME),
				 "base" => "bunch_electric_mover",
				 "class" => "",
				 "category" => __('Landshaper', BUNCH_NAME),
				 "icon" => 'fa-briefcase' ,
				 'description' => __('Show Electric Mover.', BUNCH_NAME),
				 "params" => array(
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Image", BUNCH_NAME),
								   "param_name" => "image",
								   "description" => __("Enter the Section Image to show.", BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "content",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textarea",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text', BUNCH_NAME ),
								   "param_name" => "text",
								   "description" => __('Enter The Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Link', BUNCH_NAME ),
								   "param_name" => "btn_link",
								   "description" => __('Enter The Button Link to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Button Text', BUNCH_NAME ),
								   "param_name" => "btn_text",
								   "description" => __('Enter The Button Text to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('MAin Title', BUNCH_NAME ),
								   "param_name" => "main_title",
								   "description" => __('Enter The Main Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Renovation Work', BUNCH_NAME)
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Image", BUNCH_NAME),
								   "param_name" => "image1",
								   "description" => __("Enter the Section Image to show.", BUNCH_NAME),
								   'group' => esc_html__('Renovation Work', BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title1",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Renovation Work', BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Phone No', BUNCH_NAME ),
								   "param_name" => "phone_no1",
								   "description" => __('Enter The Phone No to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Renovation Work', BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Email Address', BUNCH_NAME ),
								   "param_name" => "email1",
								   "description" => __('Enter The Email Address to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Renovation Work', BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('MAin Title', BUNCH_NAME ),
								   "param_name" => "main_title1",
								   "description" => __('Enter The Main Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('New Construction', BUNCH_NAME)
								),
								array(
								   "type" => "attach_image",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Image", BUNCH_NAME),
								   "param_name" => "image2",
								   "description" => __("Enter the Section Image to show.", BUNCH_NAME),
								   'group' => esc_html__('New Construction', BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title2",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('New Construction', BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Phone No', BUNCH_NAME ),
								   "param_name" => "phone_no2",
								   "description" => __('Enter The Phone No to Show.', BUNCH_NAME ),
								   'group' => esc_html__('New Construction', BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Email Address', BUNCH_NAME ),
								   "param_name" => "email2",
								   "description" => __('Enter The Email Address to Show.', BUNCH_NAME ),
								   'group' => esc_html__('New Construction', BUNCH_NAME)
								),
							   )
							  );								
$bunch_sc['bunch_services_grid']	=	array(
					"name" => __("Services Grid", BUNCH_NAME),
					"base" => "bunch_services_grid",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Services Grid.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Text Limit to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_service_detail_projects'] = array(
			"name" => __("Service Detail Projects", BUNCH_NAME),
			"base" => "bunch_service_detail_projects",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Service Detail Projects', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Image", BUNCH_NAME),
						   "param_name" => "image",
						   "description" => __("Enter the Section Image to show.", BUNCH_NAME)
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', BUNCH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title1",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Color Title', BUNCH_NAME ),
						   "param_name" => "title2",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Image", BUNCH_NAME),
						   "param_name" => "image1",
						   "description" => __("Enter the Section Image to show.", BUNCH_NAME),
						   'group' => esc_html__('Before Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Image Description', BUNCH_NAME ),
						   "param_name" => "img_des1",
						   "description" => __('Enter The Image Description to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Before Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title3",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Before Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Color Title', BUNCH_NAME ),
						   "param_name" => "title4",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Before Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text One', BUNCH_NAME ),
						   "param_name" => "text1",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Before Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Two', BUNCH_NAME ),
						   "param_name" => "text2",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Before Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Link', BUNCH_NAME ),
						   "param_name" => "btn_link1",
						   "description" => __('Enter The Button Link to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Before Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Text', BUNCH_NAME ),
						   "param_name" => "btn_text1",
						   "description" => __('Enter The Button Text to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Before Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Image", BUNCH_NAME),
						   "param_name" => "image2",
						   "description" => __("Enter the Section Image to show.", BUNCH_NAME),
						   'group' => esc_html__('After Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Image Description', BUNCH_NAME ),
						   "param_name" => "img_des2",
						   "description" => __('Enter The Image Description to Show.', BUNCH_NAME ),
						   'group' => esc_html__('After Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title5",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('After Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Color Title', BUNCH_NAME ),
						   "param_name" => "title6",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('After Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text One', BUNCH_NAME ),
						   "param_name" => "text3",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						   'group' => esc_html__('After Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Two', BUNCH_NAME ),
						   "param_name" => "text4",
						   "description" => __('Enter The Text to Show.', BUNCH_NAME ),
						   'group' => esc_html__('After Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Link', BUNCH_NAME ),
						   "param_name" => "btn_link2",
						   "description" => __('Enter The Button Link to Show.', BUNCH_NAME ),
						   'group' => esc_html__('After Makeover', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Text', BUNCH_NAME ),
						   "param_name" => "btn_text2",
						   "description" => __('Enter The Button Text to Show.', BUNCH_NAME ),
						   'group' => esc_html__('After Makeover', BUNCH_NAME)
						),
					),
				);
$bunch_sc['bunch_customer_feedback'] = array(
			"name" => __("customer Feedback", BUNCH_NAME),
			"base" => "bunch_customer_feedback",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show customer Feedback', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Our Best Services', BUNCH_NAME),
						),
						// params group
			            array(
			                'type' => 'param_group',
			                'value' => '',
			                'param_name' => 'accordion',
							'group' => esc_html__('Our Best Services', BUNCH_NAME),
			                'params' => array(
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Title', BUNCH_NAME ),
											'param_name' => 'title',
										),
										array(
											'type' => 'textarea',
											'value' => '',
											'heading' => esc_html__('Text', BUNCH_NAME ),
											'param_name' => 'text',
										), 
									)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title1",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title2",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Limit Of Text to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME ),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME),
								   'group' => esc_html__('Our Testimonials', BUNCH_NAME),
								),
								
							),
						);
$bunch_sc['bunch_classic_gallery']	=	array(
					"name" => __("Classic Gallery", BUNCH_NAME),
					"base" => "bunch_classic_gallery",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Classic Gallery.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Projects to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Excluded Categories ID', BUNCH_NAME ),
								   "param_name" => "exclude_cats",
								   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
									"type" => "checkbox",
									"holder" => "div",
									"class" => "",
									"heading" => __('Style Two', BUNCH_NAME ),
									"param_name" => "style_two",
									'value' => array(__('Style Two for change the container', BUNCH_NAME )=>true),
									"description" => __('Choose whether you want to show full Container.', 'SH_NAME' )
								),
							)
						);
$bunch_sc['bunch_gallery_masonry']	=	array(
					"name" => __("Gallery Masonry", BUNCH_NAME),
					"base" => "bunch_gallery_masonry",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Gallery Masonry.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Projects to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Excluded Categories ID', BUNCH_NAME ),
								   "param_name" => "exclude_cats",
								   "description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_blog_grid']	=	array(
					"name" => __("Blog Grid", BUNCH_NAME),
					"base" => "bunch_blog_grid",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Blog Grid.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_blog_list']	=	array(
					"name" => __("Blog List", BUNCH_NAME),
					"base" => "bunch_blog_list",
					"class" => "",
					"category" => __('Landshaper', BUNCH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Blog List.', BUNCH_NAME),
					"params" => array(
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Number', BUNCH_NAME ),
								   "param_name" => "num",
								   "description" => __('Enter Number of Items to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Text Limit', BUNCH_NAME ),
								   "param_name" => "text_limit",
								   "description" => __('Enter The Text Limit to Show.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __( 'Category', BUNCH_NAME ),
								   "param_name" => "cat",
								   "value" => array_flip( (array)bunch_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
								   "description" => __( 'Choose Category.', BUNCH_NAME )
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order By", BUNCH_NAME),
								   "param_name" => "sort",
								   'value' => array_flip( array('select'=>__('Select Options', BUNCH_NAME),'date'=>__('Date', BUNCH_NAME),'title'=>__('Title', BUNCH_NAME) ,'name'=>__('Name', BUNCH_NAME) ,'author'=>__('Author', BUNCH_NAME),'comment_count' =>__('Comment Count', BUNCH_NAME),'random' =>__('Random', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
								array(
								   "type" => "dropdown",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __("Order", BUNCH_NAME),
								   "param_name" => "order",
								   'value' => array_flip(array('select'=>__('Select Options', BUNCH_NAME),'ASC'=>__('Ascending', BUNCH_NAME),'DESC'=>__('Descending', BUNCH_NAME) ) ),			
								   "description" => __("Enter the sorting order.", BUNCH_NAME)
								),
							)
						);
$bunch_sc['bunch_contact_us'] = array(
			"name" => __("Contact Us", BUNCH_NAME),
			"base" => "bunch_contact_us",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Contact Us', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Contact Details', BUNCH_NAME)
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Address', BUNCH_NAME ),
						   "param_name" => "address",
						   "description" => __('Enter The Address to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Contact Details', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Phone No', BUNCH_NAME ),
						   "param_name" => "phone_no",
						   "description" => __('Enter The Phone No to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Contact Details', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Email', BUNCH_NAME ),
						   "param_name" => "email",
						   "description" => __('Enter The Email to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Contact Details', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Working Hours', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Color Title', BUNCH_NAME ),
						   "param_name" => "color_title",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Working Hours', BUNCH_NAME)
						),
						// params group
			            array(
			                'type' => 'param_group',
			                'value' => '',
			                'param_name' => 'funfacts',
							'group' => esc_html__('Working Hours', BUNCH_NAME),
			                'params' => array(
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Day', BUNCH_NAME ),
											'param_name' => 'day',
										),
										array(
											'type' => 'textfield',
											'value' => '',
											'heading' => esc_html__('Times', BUNCH_NAME ),
											'param_name' => 'times',
										), 
									)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Sub Title', BUNCH_NAME ),
								   "param_name" => "sub_title",
								   "description" => __('Enter The Sub Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Contact Form', BUNCH_NAME)
								),
								array(
								   "type" => "textfield",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Title', BUNCH_NAME ),
								   "param_name" => "title1",
								   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
								   'group' => esc_html__('Contact Form', BUNCH_NAME)
								),
								array(
								   "type" => "textarea_raw_html",
								   "holder" => "div",
								   "class" => "",
								   "heading" => __('Contact Form7 Shortcode', BUNCH_NAME ),
								   "param_name" => "contact_form",
								   "description" => __('Enter Contact Form7 Shortcode', BUNCH_NAME ),
								   'group' => esc_html__('Contact Form', BUNCH_NAME),
								),
							),
						);
$bunch_sc['bunch_google_map'] = array(
			"name" => __("Google Map", BUNCH_NAME),
			"base" => "bunch_google_map",
			"class" => "",
			"category" => __('Landshaper', BUNCH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('Show Google Map', BUNCH_NAME),
			"params" => array(
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Image", BUNCH_NAME),
						   "param_name" => "image",
						   "description" => __("Enter the Section Image to show.", BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('latitude', BUNCH_NAME ),
						   "param_name" => "lat1",
						   "description" => __('Enter The latitude to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info One ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('longitude', BUNCH_NAME ),
						   "param_name" => "long1",
						   "description" => __('Enter The longitude to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info One ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title1",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info One ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Mark Address', BUNCH_NAME ),
						   "param_name" => "mark_address1",
						   "description" => __('Enter The Mark Address to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info One ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('latitude', BUNCH_NAME ),
						   "param_name" => "lat2",
						   "description" => __('Enter The latitude to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info Two ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('longitude', BUNCH_NAME ),
						   "param_name" => "long2",
						   "description" => __('Enter The longitude to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info Two ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title2",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info Two ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Mark Address', BUNCH_NAME ),
						   "param_name" => "mark_address2",
						   "description" => __('Enter The Mark Address to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info Two ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('latitude', BUNCH_NAME ),
						   "param_name" => "lat3",
						   "description" => __('Enter The latitude to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info Three ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('longitude', BUNCH_NAME ),
						   "param_name" => "long3",
						   "description" => __('Enter The longitude to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info Three ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', BUNCH_NAME ),
						   "param_name" => "title3",
						   "description" => __('Enter The Title to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info Three ', BUNCH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Mark Address', BUNCH_NAME ),
						   "param_name" => "mark_address3",
						   "description" => __('Enter The Mark Address to Show.', BUNCH_NAME ),
						   'group' => esc_html__('Map Info Three ', BUNCH_NAME)
						),
					)
				);
																																																												
/*----------------------------------------------------------------------------*/
$bunch_sc = apply_filters( '_bunch_shortcodes_array', $bunch_sc );
	
return $bunch_sc;