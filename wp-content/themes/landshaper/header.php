<?php $options = _WSH()->option();
	landshaper_bunch_global_variable();
	$icon_href = (landshaper_set( $options, 'site_favicon' )) ? landshaper_set( $options, 'site_favicon' ) : get_template_directory_uri().'/images/favicon.png';
 ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		 <!-- Basic -->
	    <meta charset="<?php bloginfo( 'charset' ); ?>">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- Favcon -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ):?>
			<link rel="shortcut icon" type="image/png" href="<?php echo esc_url($icon_href);?>">
		<?php endif;?>
		<?php wp_head(); ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/my_custom_css.css" type="text/css" media="screen" />
	</head>
	<body <?php body_class(); ?>>
	
	
	
    <div class="page-wrapper">
	
 	<?php if(landshaper_set($options, 'preloader')):?>
    <!-- Preloader -->
    <div class="preloader"></div>
 	<?php endif;?>
    
    <?php $header = landshaper_set($options, 'header_style');
		  $header = (landshaper_set($_GET, 'header_style')) ? landshaper_set($_GET, 'header_style') : $header;
		  switch($header){
			case "header_v2":
				get_template_part('includes/modules/header_v2');
				break;
			case "header_v3":
				get_template_part('includes/modules/header_v3');
				break;
			case "header_v4":
				get_template_part('includes/modules/header_v4');
				break;
			case "header_v5":
				get_template_part('includes/modules/header_v5');
				break;
			default:
				get_template_part('includes/modules/header_v1');
			} 	
		?>
		
		
		<div id="slideout">
<!-- <img src="http://sl-agritech.com/wp-content/themes/landshaper/images/Subscribe-Button.png" alt="Subscribe" />-->
<div id="slideout_inner">

<div>

<div>
					
					<form action="http://sl-agritech.com/verify-lotno-result.php" class="needs-validation">
					
					<input type="text" style="font-weight:bold; font-size:18px;" name="lotno" id="lotno" class="form-control" value="<?php echo isset($_REQUEST['lotno'])?$_REQUEST['lotno']:''?>" placeholder="" maxlength="10" minlength="10" data-error="Minimum of 6 / Maximum of 16 characters" required>
<p style="font-size: 15px; font-family:Times;"><i>Enter your LOT NUMBER to verify</i></p>					
					</div>

							</div>
							
							 
							 
							<div>
								<div class="form-group">
									<div>
									
										<button type="submit" name="submit" value="search" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-search"></i> Validate</button>
									</div>
								</div>
							</div>
							
						</div>
					</form>
					

</div>