<div class="wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
    <div class="search-widget">
        <form action="<?php echo esc_url(home_url('/')); ?>"  method="get">
            <input type="text" name="s" placeholder="<?php esc_html_e('Enter your keyword', 'landshaper');?>" />
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div><!-- /.search-widget -->
</div><!-- /.single-sidebar -->


