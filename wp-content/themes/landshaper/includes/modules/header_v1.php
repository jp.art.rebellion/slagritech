<div id="slideout">
 <img src="http://sl-agritech.com/wp-content/themes/landshaper/images/Subscribe-Button.png" alt="Subscribe" />
<div id="slideout_inner">

<div>

<div>
					
					<form action="http://sl-agritech.com/verify-lotno-result.php" class="needs-validation">
					
					<input type="text" style="font-weight:bold; font-size:18px;" name="lotno" id="lotno" class="form-control" value="<?php echo isset($_REQUEST['lotno'])?$_REQUEST['lotno']:''?>" placeholder="" maxlength="10" minlength="10" data-error="Minimum of 6 / Maximum of 16 characters" required>
<p style="font-size: 15px; font-family:Times;"><i>Enter your LOT NUMBER to verify</i></p>					
					</div>

							</div>
							
							 
							 
							<div>
								<div class="form-group">
									<div>
									
										<button type="submit" name="submit" value="search" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-search"></i> Validate</button>
									</div>
								</div>
							</div>
							
						</div>
					</form>
					

</div>
<?php $options = _WSH()->option();
	landshaper_bunch_global_variable();
?>

<header class="header site-header">

    <div class="header-top">
        <div class="container">
            <div class="pull-left left-info contact-info">
                <ul class="list-inline contact-info-list">
                    <li>
                        <div class="inner-box">
                            <?php if(landshaper_set($options, 'welcome')):?>
                                <span><?php echo balanceTags(landshaper_set($options, 'welcome'));?></span>
                            <?php endif;?>
                        </div><!-- /.inner-box -->
                    </li>
                </ul>
            </div><!-- /.pull-left left-info -->
            <div class="pull-right contact-info right-info">
                <ul class="list-inline contact-info-list">
                    <?php if(landshaper_set($options, 'address')):?>
                    <li>
                        <div class="inner-box">
                            <i class="fa fa-home"></i><span><?php echo nl2br(wp_kses_post(landshaper_set($options, 'address')));?></span>
                        </div><!-- /.inner-box -->
                    </li><!-- adding comment for inline hack-->
                    <?php endif;?>

                    <?php if(landshaper_set($options, 'phone_no')):?>
                    <li>
                        <div class="inner-box">
                            <i class="fa fa-phone"></i><span><?php esc_html_e('', 'landshaper');?> <?php echo nl2br(wp_kses_post(landshaper_set($options, 'phone_no')));?></span>
                        </div><!-- /.inner-box -->
                    </li><!-- adding comment for inline hack-->
                    <?php endif;?>

                    <?php if(landshaper_set($options, 'openong_hours')):?>
                    <li>
                        <div class="inner-box">
                            <i class="fa fa-clock-o"></i><span><?php echo balanceTags(landshaper_set($options, 'openong_hours'));?></span>
                        </div><!-- /.inner-box -->
                    </li>
                    <?php endif;?>

                </ul>
            </div><!-- /.pull-right -->
        </div><!-- /.container -->
    </div><!-- /.header-top -->

    <nav class="navbar navbar-default header-navigation stricky">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-bar" aria-expanded="false">
                    <span class="sr-only"><?php esc_html_e('Toggle navigation', 'landshaper');?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <?php if(landshaper_set($options, 'logo_image')):?>
                    <a class="navbar-brand"  href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(landshaper_set($options, 'logo_image'));?>" alt="" title="<?php esc_html_e('Awesome Image', 'landshaper');?>"></a>
                <?php else:?>
                    <a class="navbar-brand"  href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/img/logo.png');?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>"></a>
                <?php endif;?>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-nav-bar">


                <ul class="nav navbar-nav navigation-box">
                    <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
                        'container_class'=>'navbar-collapse collapse navbar-right',
                        'menu_class'=>'nav navbar-nav',
                        'fallback_cb'=>false,
                        'items_wrap' => '%3$s',
                        'container'=>false,
                        'walker'=> new Bunch_Bootstrap_walker()
                    ) ); ?>
                </ul>
                <ul class="nav navbar-nav navbar-right right-box">
                    <?php if(landshaper_set($options, 'show_social_icons')):?>
                    <?php if($socials = landshaper_set(landshaper_set($options, 'social_media'), 'social_media')): //landshaper_set_printr($socials);?>

                        <?php foreach($socials as $key => $value):
                            if(landshaper_set($value, 'tocopy')) continue;
                        ?>
                        <li><a href="<?php echo esc_url(landshaper_set($value, 'social_link'));?>"><span class="phone-only"><?php echo balanceTags(landshaper_set($value, 'title'));?></span><i class="fa <?php echo balanceTags(landshaper_set($value, 'social_icon'));?>"></i></a></li>
                        <?php endforeach;?>

                    <?php endif;?>
                    <?php endif;?>
                    <li class="search-button">
                        <a class="search-opener" href="#"><span class="phone-only"><?php esc_html_e('Search', 'landshaper');?></span><i class="fa fa-search"></i></a>
                        <div class="sub-menu search-box">
                          <?php get_template_part('searchform2')?>
                        </div><!-- /.sub-menu -->
                    </li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>

</header><!-- /.header -->
