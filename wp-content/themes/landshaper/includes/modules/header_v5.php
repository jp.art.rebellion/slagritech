<?php $options = _WSH()->option();
	landshaper_bunch_global_variable();
?>



<header class="header header-5 site-header">
    <div class="header-top white-color-bg">
        <div class="container">
            <div class="logo text-center">
                <?php if(landshaper_set($options, 'h5_logo_image')):?>
                    <a class="navbar-brand"  href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(landshaper_set($options, 'h5_logo_image'));?>" alt="" title="<?php esc_html_e('Awesome Image', 'landshaper');?>"></a>
                <?php else:?>
                    <a class="navbar-brand"  href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/img/logo.png');?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>"></a>
                <?php endif;?>
            </div><!-- /.logo -->
            <?php if(landshaper_set($options, 'phone_no')):?>
            <div class="contact-infos">
                <p><i class="fa fa-phone"></i> &nbsp;&nbsp; <?php esc_html_e('Call Us:', 'landshaper');?> <span class="black-color-text"><?php echo nl2br(wp_kses_post(landshaper_set($options, 'phone_no')));?></span></p>
            </div><!-- /.contact-infos -->
            <?php endif;?>
            <div class="search-form">
                 <?php get_template_part('searchform3')?>
            </div><!-- /.search-form -->
        </div><!-- /.container -->
    </div><!-- /.header-top -->  
    <nav class="navbar navbar-default header-navigation stricky">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-bar" aria-expanded="false">
                    <span class="sr-only"><?php esc_html_e('Toggle navigation', 'landshaper');?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>               
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-nav-bar">
                
                <ul class="nav navbar-nav navigation-box">
                   <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
                        'container_class'=>'navbar-collapse collapse navbar-right',
                        'menu_class'=>'nav navbar-nav',
                        'fallback_cb'=>false, 
                        'items_wrap' => '%3$s', 
                        'container'=>false,
                        'walker'=> new Bunch_Bootstrap_walker()  
                    ) ); ?> 
                </ul>
                
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>

</header>