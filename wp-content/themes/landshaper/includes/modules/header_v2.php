<?php $options = _WSH()->option();
	landshaper_bunch_global_variable();
?>
 
<header class="header header-2 site-header">
	<div class="header-top">
		<div class="container">			
			<div class="pull-left contact-info right-info">
				<ul class="list-inline contact-info-list">
					<?php if(landshaper_set($options, 'phone_no')):?>
                    <li>
						<div class="inner-box">
							<i class="fa fa-phone"></i><span><?php echo nl2br(wp_kses_post(landshaper_set($options, 'phone_no')));?></span>
						</div><!-- /.inner-box -->
					</li>
                    <?php endif;?>
                    <?php if(landshaper_set($options, 'email')):?>
                    <li>
						<div class="inner-box">
							<i class="fa fa-envelope-o"></i><span><?php echo sanitize_email(landshaper_set($options, 'email'));?></span>
						</div><!-- /.inner-box -->
					</li>
                    <?php endif;?>
				</ul>
			</div><!-- /.pull-right -->
            <div class="pull-right contact-info right-info">
                <ul class="link-list pull-left list-inline">
                    <li><a href="<?php echo esc_url(landshaper_set($options, 'v2_career_link'));?>"><?php esc_html_e('Careers', 'landshaper');?></a></li>
                    <li>
                        <a href="<?php echo esc_url(landshaper_set($options, 'v2_desk_link'));?>"><?php esc_html_e('Helpdesk', 'landshaper');?></a>
                    </li><!-- comment for inline hack
                     --><li>
                        <a href="<?php echo esc_url(landshaper_set($options, 'v2_sup_link'));?>"><?php esc_html_e('Support', 'landshaper');?></a>
                    </li>
                </ul><!-- /.link-list -->
                <?php if(landshaper_set($options, 'show_shocial_icons')):?>
				<?php if($socials = landshaper_set(landshaper_set($options, 'social_media'), 'social_media')): //landshaper_printr($socials);?>
                <ul class="social-icon pull-left list-inline">
					<?php foreach($socials as $key => $value):
                        if(landshaper_set($value, 'tocopy')) continue;
                        ?>
                        <li><a href="<?php echo esc_url(landshaper_set($value, 'social_link'));?>" class="tran3s"><i class="fa <?php echo balanceTags(landshaper_set($value, 'social_icon'));?>" aria-hidden="true"></i></a></li>
                    <?php endforeach;?>
                </ul>
                <?php endif;?>
                <?php endif;?>
                
            </div><!-- /.pull-right -->
		</div><!-- /.container -->
	</div><!-- /.header-top -->
	<nav class="navbar navbar-default header-navigation stricky">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-bar" aria-expanded="false">
					<span class="sr-only"><?php esc_html_e('Toggle navigation', 'landshaper');?></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<?php if(landshaper_set($options, 'v2_logo_image')):?>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand light-logo"><img src="<?php echo esc_url(landshaper_set($options, 'v2_logo_image'));?>" alt="" title="<?php esc_html_e('Logo', 'landshaper');?>"></a>
                <?php else:?>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand light-logo"><img src="<?php echo esc_url(get_template_directory_uri().'/img/logo2.png');?>" alt="<?php esc_html_e('Logo', 'landshaper');?>"></a>
                <?php endif;?>
            </div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="main-nav-bar">
				

				<ul class="nav navbar-nav navigation-box">
					<?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
						'container_class'=>'navbar-collapse collapse navbar-right',
						'menu_class'=>'nav navbar-nav',
						'fallback_cb'=>false, 
						'items_wrap' => '%3$s', 
						'container'=>false,
						'walker'=> new Bunch_Bootstrap_walker()  
					) ); ?>
                </ul>			
				<ul class="nav navbar-nav navbar-right right-box">
					<li>
						<a href="<?php echo esc_url(landshaper_set($options, 'v2_quote_link'));?>" class="thm-btn"><?php esc_html_e('Get a Quote', 'landshaper');?></a>
					</li>
				</ul>
				
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container -->
	</nav>

</header> 