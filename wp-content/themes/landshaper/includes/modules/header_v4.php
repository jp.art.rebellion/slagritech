<?php $options = _WSH()->option();
	landshaper_bunch_global_variable();
?>

<header class="header header-4 site-header">	
    <div class="header-top  white-color-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php if(landshaper_set($options, 'h4_logo_image')):?>
                        <a class="navbar-brand"  href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(landshaper_set($options, 'h4_logo_image'));?>" alt="" title="<?php esc_html_e('Awesome Image', 'landshaper');?>"></a>
                    <?php else:?>
                        <a class="navbar-brand"  href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/img/logo.png');?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>"></a>
                    <?php endif;?>
                </div><!-- /.col-md-3 -->
                <div class="col-md-6">
                    <?php get_template_part( 'searchform5' ); ?>                    
                </div><!-- /.col-md-6 -->
                <div class="col-md-3">
                    <a href="<?php echo esc_url(landshaper_set($options, 'v4_quote_link'));?>" class="thm-btn pull-right"><?php esc_html_e('Get a Quote', 'landshaper');?></a>
                    <?php if(landshaper_set($options, 'show_shocial_icons')):?>
                    <div class="social pull-right">
                    
					<?php if($socials = landshaper_set(landshaper_set($options, 'social_media'), 'social_media')): //landshaper_set_printr($socials);?>
                        
                        <?php foreach($socials as $key => $value):
                            if(landshaper_set($value, 'tocopy')) continue;
                        ?>
                        <a href="<?php echo esc_url(landshaper_set($value, 'social_link'));?>"><i class="fa <?php echo balanceTags(landshaper_set($value, 'social_icon'));?>"></i></a></li>
                        <?php endforeach;?>
                        
                    <?php endif;?>
                    </div>
                    <?php endif;?>
                    
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-top -->
	<nav class="navbar navbar-default header-navigation stricky">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-bar" aria-expanded="false">
					<span class="sr-only"><?php esc_html_e('Toggle navigation', 'landshaper');?></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>				
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="main-nav-bar">
				
				<ul class="nav navbar-nav navigation-box">
					
                   <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
                        'container_class'=>'navbar-collapse collapse navbar-right',
                        'menu_class'=>'nav navbar-nav',
                        'fallback_cb'=>false, 
                        'items_wrap' => '%3$s', 
                        'container'=>false,
                        'walker'=> new Bunch_Bootstrap_walker()  
                    ) ); ?> 
                    
				</ul>
                <ul class="nav navbar-nav right-navigation navbar-right">
                    <?php if( function_exists( 'WC' ) ):
							global $woocommerce; ?>
                    <li class="cart-button">
                        <a href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>">
                            <span class="phone-only"><?php esc_html_e('Cart', 'landshaper');?> <?php echo balanceTags($woocommerce->cart->get_cart_contents_count()); ?></span>
                            <i class="fa fa-shopping-cart"></i>
                            <span class="count"><?php echo balanceTags($woocommerce->cart->get_cart_contents_count()); ?></span>
                        </a>
                    </li>
                    <?php endif;?>
                </ul><!-- /.nav navbar-nav right-navigation -->			
				
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container -->
	</nav>

</header>