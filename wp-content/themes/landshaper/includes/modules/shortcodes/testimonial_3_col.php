<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<section class="testimonial-style-two sec-pad">
    <div class="container">
        <div class="row">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$testimonial_meta = _WSH()->get_meta();
			?>
            <div class="col-md-4">
                <div class="single-testimonial-two">
                    <div class="text-box">
                        <p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?></p>
                    </div><!-- /.text-box -->
                    <div class="name-box">
                        <div class="icon-box">
                            <?php the_post_thumbnail('landshaper_60x60');?>
                        </div><!-- /.icon-box -->
                        <div class="title-box">
                            <h3 class="secondary-font"><?php the_title();?></h3>
                            <span class="secondary-font">- <?php echo wp_kses_post(landshaper_set($testimonial_meta, 'designation'));?></span>
                        </div><!-- /.title-box -->
                    </div><!-- /.name-box -->
                </div><!-- /.single-testimonial-two -->
            </div>
            <!-- /.col-md-4 -->
        	<?php endwhile;?>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.testimonial-style-two -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>