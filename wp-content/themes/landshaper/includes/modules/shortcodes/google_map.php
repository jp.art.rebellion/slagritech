<?php   
ob_start();
?>

<div class="google-map-wrapper">
	<div 
	  class="google-map" 
	  id="home-google-map" 
	  data-map-lat="<?php echo esc_js($lat1);?>" 
	  data-map-lng="<?php echo esc_js($long1);?>"
	  data-map-title="<?php echo esc_js($mark_address1);?>"
	  data-icon-path="<?php echo esc_url(wp_get_attachment_url($image));?>"
	  data-map-zoom="12"
	  data-markers="{
		  &quot;marker-1&quot;: [<?php echo esc_js($lat1);?>, <?php echo esc_js($long1);?>, &quot;<h4><?php echo esc_js($title1);?></h4><p><?php echo esc_js($mark_address1);?></p>&quot;],
		  
          &quot;marker-2&quot;: [<?php echo esc_js($lat2);?>, <?php echo esc_js($long2);?>, &quot;<h4><?php echo esc_js($title2);?></h4> <p><?php echo esc_js($mark_address2);?></p>&quot;],
		  
          &quot;marker-3&quot;: [<?php echo esc_js($lat3);?>, <?php echo esc_js($long3);?>, &quot;<h4><?php echo esc_js($title3);?></h4> <p><?php echo esc_js($mark_address3);?></p>&quot;]
	  }">

	 </div>
</div>


<?php return ob_get_clean();?>		