<?php
ob_start() ;?>

<section class="home-cta-box has-base-color-overlay sec-pad text-center" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img));?>')">
    <div class="container">
        <h3><?php echo balanceTags($title);?></h3>
    </div><!-- /.container -->
</section><!-- /.home-cta-box -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   