<?php
ob_start() ;?>
<div class="ls-padding">
	<div class="container">
        <div class="row sec-pad">
            <div class="col-md-8">
                <div class="add-banner">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="pull-left img-box wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <img src="<?php echo esc_url(wp_get_attachment_url($image));?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
                            </div><!-- /.pull-left img-box -->
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6">
                            <div class="pull-right text-box text-center">
                                <h4><?php echo balanceTags($sub_title);?></h4>
                                <h3>
                                    <?php echo balanceTags($contents);?>
                                </h3>
                                <p><?php echo balanceTags($text);?></p>
                                <a href="<?php echo esc_url($btn_link);?>"><?php echo balanceTags($btn_text);?><i class="fa fa-caret-right"></i></a>
                            </div><!-- /.pull-right text-box -->  
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->
                </div><!-- /.add-banner -->
            </div><!-- /.col-md-8 -->
            <div class="col-md-4">
                <div class="single-sidebar">
                    <div class="agent-widget">
                        <div class="single-agent-widget">
                            <h3><?php echo balanceTags($main_title);?></h3>
                            <div class="info-box">
                                <div class="icon-box">
                                    <img src="<?php echo esc_url(wp_get_attachment_url($image1));?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
                                </div><!-- /.icon-box -->
                                <div class="text-box">
                                    <h4><?php echo balanceTags($title1);?></h4>
                                    <ul>
                                        <li><i class="fa fa-phone"></i><?php echo nl2br(wp_kses_post($phone_no1));?></li>
                                        <li><i class="fa fa-envelope"></i> <?php echo sanitize_email($email1);?></li>
                                    </ul>
                                </div><!-- /.text-box -->
                            </div><!-- /.info-box -->
                        </div><!-- /.single-agent-widget -->
                        <div class="single-agent-widget">
                            <h3><?php echo balanceTags($main_title1);?></h3>
                            <div class="info-box">
                                <div class="icon-box">
                                    <img src="<?php echo esc_url(wp_get_attachment_url($image2));?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
                                </div><!-- /.icon-box -->
                                <div class="text-box">
                                    <h4><?php echo balanceTags($title2);?></h4>
                                    <ul>
                                        <li><i class="fa fa-phone"></i><?php echo nl2br(wp_kses_post($phone_no2));?></li>
                                        <li><i class="fa fa-envelope"></i> <?php echo sanitize_email($email2);?></li>
                                    </ul>
                                </div><!-- /.text-box -->
                            </div><!-- /.info-box -->
                        </div><!-- /.single-agent-widget -->
                    </div><!-- /.agent-widget -->
                </div><!-- /.single-sidebar -->
            </div><!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div>
</div>
<?php
 $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>