<?php
ob_start() ;?>

<section class="rfe-box gray-color-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="sec-title pb0 semi-medium">
                    <div class="inner">
                        <span class="tag-line"><?php echo balanceTags($sub_title);?></span>
                        <h2><?php echo balanceTags($title);?></h2>
                        <span class="decor-line"></span>
                    </div><!-- /.inner-->
                </div><!-- /.sec-title-->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-9">
                <?php echo do_shortcode(bunch_base_decode($our_estimation));?>
            </div><!-- /.col-lg-9 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.ref-box -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   