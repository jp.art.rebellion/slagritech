<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<section class="service-box-two sec-pad pt0 no-outer-border">
    <div class="container">
        <div class="row">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$services_meta = _WSH()->get_meta();
			?>
            <div class="col-md-4">
                <div class="single-service-box-two">
                    <div class="icon-box">
                        <i class="fa <?php echo str_replace("icon", "", landshaper_set($services_meta, 'fontawesome'));?>"></i>
                    </div><!-- /.icon-box -->
                    <div class="text-box">
                        <h3><?php the_title();?></h3>
                        <p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?></p>
                    </div><!-- /.text-box -->
                </div><!-- /.single-service-box-two -->
            </div>
            <!-- /.col-md-4 -->
        	<?php endwhile;?>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.sec-pad -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>