<?php
ob_start() ;?>

<div class="sec-title medium">        
    <h2><?php echo balancetags($contents);?></h2>
</div><!-- /.sec-title-->
<img src="<?php echo esc_url(wp_get_attachment_url($image));?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
<p><?php echo balanceTags($text);?></p>		        
<br />

<div class="sec-title medium">        
    <h2><span class="black-color-text"><?php echo balanceTags($title1);?></span> <span class="base-color-text"><?php echo balanceTags($title2);?></span></h2>
</div>

<div class="row">
    <div class="col-md-5">
        <img src="<?php echo esc_url(wp_get_attachment_url($image1));?>" class="img-responsive" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
        <div class="box">
            <p><?php echo balanceTags($img_des1);?></p>
        </div><!-- /.box -->
    </div><!-- /.col-md-5 -->
    <div class="col-md-7">
        <div class="sec-title medium pb0">        
            <h2><span class="black-color-text"><?php echo balanceTags($title3);?></span> <span class="base-color-text"><?php echo balanceTags($title4);?></span></h2>
        </div>
        <p><?php echo balanceTags($text1);?></p>
        <p><?php echo balanceTags($text2);?></p>
        <a class="know-more" href="<?php echo esc_url($btn_link1);?>"><?php echo balanceTags($btn_text1);?> <i class="fa fa-caret-right"></i></a>
    </div><!-- /.col-md-7 -->
</div><!-- /.row -->

<br />
<br />

<div class="row">		        	
    <div class="col-md-7">
        <div class="sec-title medium pb0">        
            <h2><span class="black-color-text"><?php echo balanceTags($title5);?></span> <span class="base-color-text"><?php echo balanceTags($title6);?></span></h2>
        </div>
        <p><?php echo balanceTags($text3);?></p>
        <p><?php echo balanceTags($text4);?></p>
        <a class="know-more" href="<?php echo esc_url($btn_link2);?>"><?php echo balanceTags($btn_text2);?> <i class="fa fa-caret-right"></i></a>
    </div><!-- /.col-md-7 -->
    <div class="col-md-5">
        <img src="<?php echo esc_url(wp_get_attachment_url($image2));?>" class="img-responsive" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
        <div class="box">
            <p><?php echo balanceTags($img_des2);?></p>
        </div><!-- /.box -->
    </div><!-- /.col-md-5 -->
</div><!-- /.row -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   