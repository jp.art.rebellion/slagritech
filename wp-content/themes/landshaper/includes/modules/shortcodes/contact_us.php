<?php
ob_start() ;?>

<section class="contact-section sec-pad contact-page">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="footer-widget contact-widget ">
					<div class="sec-title semi medium">        
			            <h2><?php echo balanceTags($contents);?></h2>
			        </div><!-- /.sec-title-->
			        <ul class="contact-infos">
						<li>
							<div class="icon-box">
								<i class="lsp-icon-signs"></i>
							</div><!-- /.icon-box -->
							<div class="text-box">
								<p><?php echo nl2br(wp_kses_post($address));?></p>
							</div><!-- /.text-box -->
						</li>
						<li>
							<div class="icon-box">
								<i class="lsp-icon-technology"></i>
							</div><!-- /.icon-box -->
							<div class="text-box">
								<p><?php esc_html_e('Call us:', 'landshaper');?> <?php echo balanceTags($phone_no);?></p>
							</div><!-- /.text-box -->
						</li>
						<li>
							<div class="icon-box">
								<i class="lsp-icon-note"></i>
							</div><!-- /.icon-box -->
							<div class="text-box">
								<p><?php esc_html_e('Mail us :', 'landshaper');?> <?php echo sanitize_email($email);?>  </p>
							</div><!-- /.text-box -->
						</li>						
					</ul><!-- /.contact-infos -->						
		        </div><!-- /.footer-widget -->
		        <div class="footer-widget working-hrs-widget ">
					<div class="sec-title semi medium">        
			            <h2><span class="black-color-text"><?php echo balanceTags($title);?></span> <span class="base-color-text"><?php echo balanceTags($color_title);?></span></h2>
			        </div><!-- /.sec-title-->
			        <ul class="working-hrs">
			        	<?php $skills_array = (array)json_decode(urldecode($funfacts));
							if( $skills_array && is_array($skills_array) ): 
							foreach( (array)$skills_array as $value ):
						?>
                        <li><?php echo balanceTags(landshaper_set( $value, 'day' )); ?> <span><?php echo balanceTags(landshaper_set( $value, 'times' )); ?></span></li>
			        	<?php endforeach; endif;?>
			        </ul><!-- /.owrking-hrs -->
		        </div><!-- /.footer-widget -->
			</div><!-- /.col-md-4 -->
			<div class="col-md-8">
				<div class="sec-title">
					<div class="inner">
						<span class="tag-line"><?php echo balanceTags($sub_title);?></span>
						<h2><?php echo balanceTags($title1);?></h2>
						<span class="decor-line"></span>
					</div><!-- /.inner-->
				</div><!-- /.sec-title-->
				<?php echo do_shortcode(bunch_base_decode($contact_form));?>
			</div><!-- /.col-md-8 -->
		</div><!-- /.row -->
	</div><!-- /.containerq -->
</section><!-- /.contact-section sec-pad -->
    
<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   