<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_faqs' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['faqs_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
   
<?php if($query->have_posts()):  ?>   

<section class="sec-pad single-faq-wrapper">
	<div class="container">
		<div class="row">
			<?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$faq_meta = _WSH()->get_meta();
			?>
            <div class="col-md-6">
				<div class="single-faq-box">
					<h3><?php the_title();?></h3>
					<p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?></p>
				</div><!-- /.single-faq-box -->
			</div><!-- /.col-md-6 -->
            <?php endwhile;?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.sec-pad -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>