<?php
ob_start() ;?>

<section class="about-video sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about-tab-text-box">
                    <div class="qouted-text">                            
                        <p>
                            <span class="qoute-top"><?php esc_html_e('“', 'landshaper');?></span>
                            <?php echo balanceTags($title);?>
                            <span class="qoute-bottom"><?php esc_html_e('”', 'landshaper');?></span>
                        </p>                            
                    </div><!-- /.qouted-text -->
                    <br />
                    <br />
                    <p><?php echo balanceTags($text1);?></p>
                    <p><?php echo balanceTags($text2);?></p>
                    <img src="<?php echo esc_url(wp_get_attachment_url($sign_img));?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
                </div><!-- /.about-tab-text-box -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <div class="video-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <img src="<?php echo esc_url(wp_get_attachment_url($video_img));?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
                    <div class="overlay">
                        <div class="box">
                            <div class="content text-center">
                                <a class="video-popup" href="<?php echo esc_url($video_link);?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/textures/play-btn.png" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/></a>
                            </div><!-- /.content -->
                        </div><!-- /.box -->
                    </div><!-- /.overlay -->
                </div><!-- /.video-box -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-video sec-pad -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   