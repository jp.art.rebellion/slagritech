<?php
   ob_start();
   $mail_salt = function_exists('_bunch_generate_salt' ) ? _bunch_generate_salt( $email ) : $email;
?>

<section class="appoinment-section sec-pad pb0">
	<div class="container">
		<div class="sec-title">
            <div class="inner">
                <span class="tag-line"><?php echo balanceTags($title);?></span>
                <h2><?php echo balanceTags($title1);?></h2>
                <span class="decor-line"></span>
            </div><!-- /.inner-->
        </div><!-- /.sec-title-->
        <div id="apt_message"></div>
        <form id="appointment_form" method="post" name="appointment_form" action="<?php echo admin_url( 'admin-ajax.php?action=_bunch_ajax_callback&amp;subaction=appointment_form_submit'); ?>" class="contact-form">
	        <div class="row">
	        	<div class="col-md-8">
	        		<div class="row">
	        			<div class="col-md-6">
	        				<div class="form-grp">
			                    <input type="text" name="apt_customer_name" id="apt_customer_name" placeholder="<?php esc_html_e('Your Name *', 'landshaper');?>" />
			                </div><!-- /.form-grp -->	        		
	        			</div><!-- /.col-md-6 -->	
	        			<div class="col-md-6">
	        				<div class="form-grp">
			                    <input type="text" name="apt_customer_email" id="apt_customer_email" placeholder="<?php esc_html_e('Your Mail * ', 'landshaper');?>" />
			                </div><!-- /.form-grp -->	        		
	        			</div><!-- /.col-md-6 -->	
	        			<div class="col-md-6">
	        				<div class="form-grp">
			                    <input type="text" name="apt_customer_phone" id="apt_customer_phone" placeholder="<?php esc_html_e('Phone Number', 'landshaper');?> " />
			                </div><!-- /.form-grp -->	        		
	        			</div><!-- /.col-md-6 -->	
	        			<div class="col-md-6">
	        				<div class="form-grp">
			                    <input type="text" name="apt_customer_address" id="apt_customer_address" placeholder="<?php esc_html_e('Address', 'landshaper');?>" />
			                </div><!-- /.form-grp -->	        		
	        			</div><!-- /.col-md-6 -->	
	        			<div class="col-md-6">
	        				<div class="form-grp">
			                    <input type="text" name="apt_date" id="apt_date" placeholder="<?php esc_html_e('Date of Appoinment', 'landshaper');?>" />
			                </div><!-- /.form-grp -->	        		
	        			</div><!-- /.col-md-6 -->	
	        			<div class="col-md-6">
	        				<div class="form-grp">
			                    <input type="text" name="apt_time" id="apt_time" placeholder="<?php esc_html_e('Time of Appoinment', 'landshaper');?>" />
			                </div><!-- /.form-grp -->	        		
	        			</div><!-- /.col-md-6 -->	
	        			<div class="col-md-12">
	        				<div class="form-grp mb0">
                        		<textarea name="apt_customer_message" id="apt_customer_message" placeholder="<?php esc_html_e('Your Message', 'landshaper');?>"></textarea>
                        	</div><!-- /.form-grp -->
	        			</div><!-- /.col-md-6 -->	
	        		</div><!-- /.row -->
					
	        	</div><!-- /.col-md-8 -->
	        	<div class="col-md-4">
	        		<div class="feature-list">
	        			<label><?php esc_html_e('Select Your Service:', 'landshaper');?></label>
	        			<ul>
	        				<?php $services_needed = explode("\n",$services_needed_str);?>
							<?php foreach($services_needed as $service):?>
                            <li><input type="checkbox" name="apt_services_needed" class="apt_services_needed" value="<?php echo esc_attr(str_replace("<br />","",$service));?>" /> <span><?php echo esc_attr(str_replace("<br />","",$service));?></span></li>
	        				<?php endforeach;?>
	        			</ul>
	        		</div><!-- /.feature-list -->
	        		<div class="form-grp mb0">
                		<input type="hidden" name="receiver_email" id="receiver_email" value="<?php echo esc_attr( $mail_salt ); ?>"  />
                        <button type="submit" class="thm-btn"><?php esc_html_e('Make an Appoinment', 'landshaper');?></button>
                	</div><!-- /.form-grp -->
	        	</div><!-- /.col-md-4 -->
	        </div><!-- /.row -->
        </form>
        
	</div><!-- /.container -->
</section>

<?php return ob_get_clean();?>		