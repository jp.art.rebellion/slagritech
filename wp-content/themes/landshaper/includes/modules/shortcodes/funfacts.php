<?php
ob_start() ;?>

<section class="sec-pad fact-wrapper has-gray-texture" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($image))?>')">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="sec-title semi">
                    <h2><?php echo balanceTags($contents);?></h2>
                </div><!-- /.sec-title-->
            </div><!-- /.col-lg-3 -->
            <div class="col-lg-9">
                <div class="row">
                    <?php $skills_array = (array)json_decode(urldecode($funfacts));
						if( $skills_array && is_array($skills_array) ): 
						foreach( (array)$skills_array as $value ):
					?>
                    <div class="col-md-4">
                        <div class="single-fact-box clearfix">
                            <div class="text-box pull-left">
                                <div class="count-box">
                                    <span class="counter"><?php echo balanceTags(landshaper_set( $value, 'counter' )); ?></span><span class="symbol"><?php echo balanceTags(landshaper_set( $value, 'symbol' )); ?></span>
                                </div><!-- /.count-box -->
                                <p><?php echo balanceTags(landshaper_set( $value, 'title' )); ?></p>
                            </div><!-- /.text-box -->
                            <div class="icon-box pull-right">
                                <i class="<?php echo esc_attr(str_replace("icon ", "", landshaper_set( $value, 'icon' )));?>"></i>
                            </div><!-- /.pull-right -->
                        </div><!-- /.single-fact-box -->
                    </div><!-- /.col-md-4 -->
                    <?php endforeach; endif;?>
                    
                </div><!-- /.row -->
            </div><!-- /.col-lg-9 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.sec-pad fact-wrapper -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   