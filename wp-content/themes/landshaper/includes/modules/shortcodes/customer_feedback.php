<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<div class="row sec-pad pb0">
    <div class="col-md-6">
        <div class="clearfix">
            <div class="sec-title medium ">        
                <h2><?php echo balanceTags($contents);?></h2>
            </div><!-- /.sec-title-->		                
            <div class="panel-group accordion style-three mb0" id="accordion-one" role="tablist">
                <?php $skills_array = (array)json_decode(urldecode($accordion));
                    if( $skills_array && is_array($skills_array) ): 
                    foreach( (array)$skills_array as $key => $value ):
                ?>
                <div class="panel panel-default">                       
                    <a role="button" data-toggle="collapse" data-parent="#accordion-one" href="#accordion-one-collapse-one<?php echo balanceTags($key);?>" class="<?php if(!($key == 1)) echo "collapsed";?>"><?php echo balanceTags(landshaper_set( $value, 'title' )); ?></a>

                    <div id="accordion-one-collapse-one<?php echo balanceTags($key);?>" class="panel-collapse collapse <?php if($key == 1) echo "in";?>" role="tabpanel">
                        <div class="inner-box">
                            <p><?php echo balanceTags(landshaper_set( $value, 'text' )); ?></p>
                        </div>
                    </div>
                </div>
                <?php $key++; endforeach; endif;?>      
            </div>
        </div><!-- /.clearfix -->
    </div><!-- /.col-md-6 -->
    <div class="col-md-6">
        <div class="sec-title medium ">        
            <h2><span class="black-color-text"><?php echo balanceTags($title1);?></span> <span class="base-color-text"><?php echo balanceTags($title2);?></span></h2>
        </div><!-- /.sec-title-->		                
        <div class="testimonial-style-two with-carousel navigation-at-top">
            <div class="testimonial-widget-carousel">                                                
                <?php while($query->have_posts()): $query->the_post();
                    global $post ; 
                    $testimonial_meta = _WSH()->get_meta();
                ?>
                <div class="item">
                    <div class="single-testimonial-two mt0">
                        <div class="text-box">
                            <p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?> </p>
                        </div><!-- /.text-box -->
                        <div class="name-box">
                            <div class="icon-box">
                                <?php the_post_thumbnail('landshaper_60x60');?>
                            </div><!-- /.icon-box -->
                            <div class="title-box">
                                <h3 class="secondary-font"><?php the_title();?></h3>
                                <span class="secondary-font"><?php esc_html_e('-', 'landshaper');?> <?php echo wp_kses_post(landshaper_set($testimonial_meta, 'designation'));?></span>
                            </div><!-- /.title-box -->
                        </div><!-- /.name-box -->
                    </div><!-- /.single-testimonial-two -->
                </div><!-- /.item -->
                <?php endwhile;?>
            </div><!-- /.row -->
        </div>
    </div><!-- /.col-md-6 -->
</div><!-- /.row -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>