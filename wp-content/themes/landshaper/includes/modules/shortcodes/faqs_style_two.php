<?php
ob_start() ;?>

<section class="sec-pad pt0 accordion-box">
	<div class="container">
		<div class="col-md-6">
			<div class="sec-title medium pb0">        
	            <h2><?php echo balanceTags($contents);?></h2>
	        </div><!-- /.sec-title-->
			<div class="panel-group accordion" id="accordion-one" role="tablist">
				<?php $skills_array = (array)json_decode(urldecode($accordion_one));
					if( $skills_array && is_array($skills_array) ): 
					foreach( (array)$skills_array as $key => $value ):
				?>
                <div class="panel panel-default">						
					<a role="button" data-toggle="collapse" data-parent="#accordion-one" href="#accordion-one-collapse-one<?php echo balanceTags($key);?>" class="<?php if(!($key == 1)) echo "collapsed";?>"><?php echo balanceTags(landshaper_set( $value, 'title' )); ?></a>

					<div id="accordion-one-collapse-one<?php echo balanceTags($key);?>" class="panel-collapse collapse <?php if($key == 1) echo "in";?>" role="tabpanel">
						<div class="inner-box">
							<p><?php echo balanceTags(landshaper_set( $value, 'text1' )); ?></p>
							<p><?php echo balanceTags(landshaper_set( $value, 'text2' )); ?></p>
						</div>
					</div>
				</div>
                <?php $key++; endforeach; endif;?>					
			</div>
		</div><!-- /.col-md-6 -->
		<div class="col-md-6">		
			<div class="sec-title medium pb0">        
	            <h2><span class="black-color-text"><?php echo balanceTags($title1);?></span> <span class="base-color-text"><?php echo balanceTags($title2);?></span></h2>
	        </div><!-- /.sec-title-->
			<div class="panel-group accordion style-two" id="accordion-two" role="tablist">
				<?php $skills_array = (array)json_decode(urldecode($accordion_two));
					if( $skills_array && is_array($skills_array) ): 
					foreach( (array)$skills_array as $key => $value ):
				?>
                <div class="panel panel-default">						
					<a role="button" data-toggle="collapse" data-parent="#accordion-two" href="#accordion-two-collapse-one<?php echo balanceTags($key);?>" class="<?php if(!($key == 0)) echo "collapsed";?>"><?php echo balanceTags(landshaper_set( $value, 'title1' )); ?></a>

					<div id="accordion-two-collapse-one<?php echo balanceTags($key);?>" class="panel-collapse collapse <?php if($key == 0) echo "in";?>" role="tabpanel">
						<div class="inner-box">
							<p><?php echo balanceTags(landshaper_set( $value, 'text3' )); ?> </p>
							<p><?php echo balanceTags(landshaper_set( $value, 'text4' )); ?></p>
						</div>
					</div>
				</div>
                <?php $key++; endforeach; endif;?>			
			</div>
		</div><!-- /.col-md-6 -->
	</div><!-- /.container -->
</section><!-- /.sec-pad -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   