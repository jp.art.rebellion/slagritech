<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['team_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>  
<?php if($query->have_posts()):  ?>   

<section class="sec-pad team-wrapper">
    <div class="container">
        <div class="sec-title semi text-center">        
            <h2><?php echo balanceTags($contents);?></h2>
        </div><!-- /.sec-title-->
        <div class="row">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$team_meta = _WSH()->get_meta();
			?>
            <div class="col-md-3">
                <div class="single-team-member wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="img-box">
                        <?php the_post_thumbnail('landshaper_270x240');?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <?php if($socials = landshaper_set($team_meta, 'bunch_team_social')):?>
                                    <ul class="list-inline">
                                        <?php foreach($socials as $key => $value):?>
                                        	<li><a href="<?php echo esc_url(landshaper_set($value, 'social_link'));?>"><i class="fa <?php echo esc_attr(landshaper_set($value, 'social_icon'));?>"></i></a></li>
                                        <?php endforeach;?>
                                    </ul><!-- /.list-inline -->
                                    <?php endif;?>
                                </div><!-- /.content -->
                            </div><!-- /.box -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                    <span class="thm-btn"><?php echo wp_kses_post(landshaper_set($team_meta, 'designation'));?></span>
                    <h3><?php the_title();?></h3>
                    <p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?></p>
                </div><!-- /.single-team-member -->
            </div><!-- /.col-md-3 -->
        	<?php endwhile;?>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.sec-pad -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>