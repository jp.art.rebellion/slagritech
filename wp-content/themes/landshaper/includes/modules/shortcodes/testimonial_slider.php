<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<section class="client-testimonials sec-pad has-gray-texture2" style="background-image:url('<?php echo esc_url(wp_get_attachment_url($bg_img));?>')">
    <div class="container">
        <div class="sec-title semi text-center">        
            <h2><span class="black-color-text"><?php echo balanceTags($title1);?></span> <span class="base-color-text"><?php echo balanceTags($title2);?></span></h2>
        </div><!-- /.sec-title-->
        <div class="client-testimonial-carousel owl-carousel owl-theme">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$testimonial_meta = _WSH()->get_meta();
			?>
            <div class="item">
                <div class="single-testimonial-home">
                    <div class="icon-box">
                        <?php the_post_thumbnail('landshaper_80x80');?>
                    </div><!-- /.icon-box -->
                    <div class="text-box">
                        <p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?></p>
                        <h3><?php the_title();?> <span><?php esc_html_e('-', 'landshaper');?> <?php echo wp_kses_post(landshaper_set($testimonial_meta, 'designation'));?></span></h3>
                    </div><!-- /.text-box -->
                </div><!-- /.single-testimonial-home -->
            </div>
            <!-- /.item -->
            <?php endwhile;?>
        </div><!-- /.client-testimonial-carousel -->
    </div><!-- /.container -->
</section><!-- /.client-testimonials -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>