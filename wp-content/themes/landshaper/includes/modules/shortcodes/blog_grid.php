<?php  
   global $post ;
   $count = 0;
   $paged = get_query_var('paged');
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order, 'paged'=>$paged);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<section class="news-wrapper sec-pad news-grid-page">
    <div class="container">        
        <div class="row">
        	<?php while($query->have_posts()): $query->the_post();
                global $post ; 
                $post_meta = _WSH()->get_meta();
            ?>
            <div class="col-md-4">
                <div class="single-news-post">
                    <div class="img-box">
                        <?php the_post_thumbnail('landshaper_370x200');?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url(get_permalink(get_the_id()));?>" class="thm-btn"><?php esc_html_e('Read More', 'landshaper');?></a>
                                </div><!-- /.content -->
                            </div><!-- /.box -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                    <ul class="meta-info list-inline">
                        <li>
                            <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php esc_html_e('by', 'landshaper');?> <?php the_author();?> </a>
                        </li><!-- adding this comment for inline hack
                        --><li>
                            <a href="<?php echo esc_url(get_month_link(get_the_date('Y'), get_the_date('m'))); ?>"><?php echo get_the_date('M d, Y');?></a>
                        </li>
                    </ul><!-- /.meta-info list-inline -->
                    <h3><a href="<?php echo esc_url(get_permalink(get_the_id()));?>"><?php the_title();?></a></h3>
                </div><!-- /.single-news-post -->
            </div><!-- /.col-md-4 -->
        	<?php endwhile;?>
            
        </div><!-- /.row -->
        <div class="post-navigation list-inline text-center">
			<?php landshaper_the_pagination(array('total'=>$query->max_num_pages, 'next_text' => '<i class="fa fa-caret-right"></i>', 'prev_text' => '<i class="fa fa-caret-left"></i>')); ?>
        </div><!-- /.post-navigation -->
    </div><!-- /.container -->
</section><!-- /.news-wrapper -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>