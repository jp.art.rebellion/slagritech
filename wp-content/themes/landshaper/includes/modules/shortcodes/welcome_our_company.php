<?php
ob_start() ;?>

<section class="service-box-one-wrapper sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="left-text">
                    <div class="sec-title outer-stroke">
                        <div class="inner">
                            <span class="tag-line"><?php echo balancetags($sub_title);?></span>
                            <h2><?php echo balancetags($title);?></h2>
                            <span class="decor-line"></span>
                        </div><!-- /.inner-->
                    </div><!-- /.sec-title-->
                    <div class="text-box">
                        <p><?php echo balancetags($text1);?></p>
                        <p><?php echo balancetags($text2);?></p>
                        <a href="<?php echo esc_url($btn_link);?>" class="thm-btn"><?php echo balancetags($btn_text);?></a>
                    </div><!-- /.text-box -->
                </div><!-- /.left-text -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-8">
                <div class="row single-service-box-wrapper">
                    <?php $skills_array = (array)json_decode(urldecode($services));
						if( $skills_array && is_array($skills_array) ): 
						foreach( (array)$skills_array as $value ):
					?>
                    <div class="col-md-6">
                        <div class="single-service-box-one wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="img-box">
                                <img src="<?php echo esc_url(wp_get_attachment_url(landshaper_set( $value, 'image' ))); ?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
                                <div class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <a href="<?php echo esc_url(landshaper_set( $value, 'btn_link' )); ?>" class="thm-btn"><?php echo (landshaper_set( $value, 'btn_text' )); ?></a>
                                        </div><!-- /.content -->
                                    </div><!-- /.box -->
                                </div><!-- /.overlay -->
                            </div><!-- /.img-box -->
                            <h3><?php echo balanceTags(landshaper_set( $value, 'title' )); ?></h3>
                            <p><?php echo balanceTags(landshaper_set( $value, 'text' )); ?></p>
                        </div><!-- /.single-service-box-one -->
                    </div>
                    <?php endforeach; endif;?>
                    <!-- /.col-md-6 -->
                </div><!-- /.row -->
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.service-box-one-wrapper -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   