<?php
ob_start() ;?>

<section class="sec-pad pricing-plan-wrapper">
    <div class="container">        
        <?php if($title):?>
        <div class="sec-title semi text-center">        
            <h2><span class="black-color-text"><?php echo balanceTags($title);?></span></h2>
        </div><!-- /.sec-title-->
        <?php endif;?>
        <div class="row">
            <?php $skills_array = (array)json_decode(urldecode($pricing_table));
				if( $skills_array && is_array($skills_array) ): 
				foreach( (array)$skills_array as $value ):
			?>
            <div class="col-md-3">
                <div class="single-price-box text-center gray-color-bg">
                    <div class="price-box base-color-bg">
                        <p><span><?php echo balanceTags(landshaper_set( $value, 'currency' )); ?></span> <?php echo balanceTags(landshaper_set( $value, 'price' )); ?></p>
                    </div><!-- /.price-box -->
                    <div class="name-box">
                        <h3><?php echo balanceTags(landshaper_set( $value, 'title' )); ?></h3>
                        <span><?php echo balanceTags(landshaper_set( $value, 'duration' )); ?></span>
                    </div><!-- /.name-box -->
                    <div class="feature-box">
                        <ul>
                            <?php $fearures = explode("\n",landshaper_set($value, 'feature_str'));?>
							<?php foreach($fearures as $feature):?>
                                <li><?php echo balanceTags($feature ); ?></li>
                            <?php endforeach;?>
                        </ul>
                    </div><!-- /.feature-box -->
                    <div class="button-box">
                        <a href="<?php echo esc_url(landshaper_set( $value, 'btn_link' )); ?>" class="thm-btn"><?php echo balanceTags(landshaper_set( $value, 'btn_text' )); ?></a>
                    </div><!-- /.button-box -->
                </div><!-- /.single-price-box -->
            </div><!-- /.col-md-3 -->
            <?php endforeach; endif;?>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.sec-pad pricing-plan-wrapper -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   