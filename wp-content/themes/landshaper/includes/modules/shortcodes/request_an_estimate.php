<?php
ob_start() ;?>

<div class="google-map-wrapper">
    <div 
        class="google-map" 
        id="home-google-map" 
        data-map-lat="<?php echo esc_js($lat);?>" 
        data-map-lng="<?php echo esc_js($long);?>"
        data-map-title="<?php echo esc_js($mark_title);?>"
        data-map-zoom="11">
    
    </div>

    <div class="request-qoute-box sec-pad in-map">
        <div class="container">
            <div class="col-md-6 pull-right">
                <div class="over-map wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="sec-title outer-stroke">
                        <div class="inner">
                            <span class="tag-line"><?php echo balanceTags($sub_title);?></span>
                            <h2><?php echo balanceTags($title);?></h2>
                            <span class="decor-line"></span>
                        </div><!-- /.inner-->
                    </div><!-- /.sec-title-->
                    <div class="inner-box">
                        <?php echo do_shortcode(bunch_base_decode($contact_form));?>
                        
                        <p><label><?php esc_html_e('For Business :', 'landshaper');?></label>  <?php echo balanceTags($contents);?></p>
                        <p><label><?php esc_html_e('Office Hours :', 'landshaper');?></label> <?php echo balanceTags($office_hours);?></p>
                    </div><!-- /.inner-box -->
                </div><!-- /.over-map -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.container -->
    </div><!-- /.request-qoute-box -->

</div><!-- /.google-map-wrapper -->
    
<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   