<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<section class="service-box-two sec-pad">
    <div class="container">
        <div class="clearfix">
            <div class="pull-left">
                <div class="sec-title">
                    <div class="inner">
                        <span class="tag-line"><?php echo balanceTags($sub_title);?></span>
                        <h2><?php echo balanceTags($title);?></h2>
                        <span class="decor-line"></span>
                    </div><!-- /.inner-->
                </div><!-- /.sec-title-->
            </div><!-- /.pull-left -->
            <div class="pull-right">
                <a href="<?php echo esc_url($btn_link);?>" class="thm-btn"><?php echo balanceTags($btn_text);?></a>
            </div><!-- /.pull-right -->
        </div><!-- /.clearfix -->
        <div class="row">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$services_meta = _WSH()->get_meta();
			?>
            <div class="col-md-4">
                <div class="single-service-box-two">
                    <div class="icon-box wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <i class="fa <?php echo str_replace("icon", "", landshaper_set($services_meta, 'fontawesome'));?>"></i>
                    </div><!-- /.icon-box -->
                    <div class="text-box">
                        <h3><?php the_title();?></h3>
                        <p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?></p>
                    </div><!-- /.text-box -->
                </div><!-- /.single-service-box-two -->
            </div>
            <?php endwhile;?>
            <!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.service-box-two sec-pad -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>