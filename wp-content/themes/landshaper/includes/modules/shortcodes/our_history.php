<?php
ob_start() ;?>

<section class="about-tab-wrapper sec-pad pt0">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <ul class="tab-title-box" role="tablist">
                    <?php $skills_array = (array)json_decode(urldecode($our_history));
						if( $skills_array && is_array($skills_array) ): 
						foreach( (array)$skills_array as $key => $value ):
					?>
                    <li data-tab-name="1986<?php echo balanceTags($key);?>" class="<?php if($key == 1) echo "active";?>">
                        <a href="#1986<?php echo esc_attr($key);?>" aria-controls="1986<?php echo balanceTags($key);?>" role="tab" data-toggle="tab"><span><?php echo balanceTags(landshaper_set( $value, 'years' )); ?></span><?php echo balanceTags(landshaper_set( $value, 'title' )); ?></a>
                    </li>
                    <?php $key++; endforeach; endif;?> 
                </ul><!-- /.tab-title-box -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-8">
                <div class="tab-content">
                    <?php $skills_array = (array)json_decode(urldecode($our_history));
						if( $skills_array && is_array($skills_array) ): 
						foreach( (array)$skills_array as $key => $value ):
					?>
                    <div class="single-tab-box tab-pane fade in <?php if($key == 1) echo "active";?>" id="1986<?php echo balanceTags($key);?>">
                        <div class="image-box">
                            <img src="<?php echo esc_url(wp_get_attachment_url(landshaper_set( $value, 'img' ))); ?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
                        </div><!-- /.image-box -->
                        <div class="text-box">
                            <h3><?php echo balanceTags(landshaper_set( $value, 'title' )); ?></h3>
                            <p><?php echo balanceTags(landshaper_set( $value, 'text' )); ?></p>
                        </div><!-- /.text-box -->
                    </div>
                    <!-- /.single-tab-box -->
                    <?php $key++; endforeach; endif;?>
                </div><!-- /.tab-content -->
            </div><!-- /.col-md-8 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-tab-wrapper -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   