<?php  
   $count = 1;
   $query_args = array('post_type' => 'product' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   
   if( $cat ) $query_args['product_cat'] = $cat;
   $query = new WP_Query($query_args) ; 
   
   ob_start() ;?>
   
<?php if($query->have_posts()):  ?>   

<section class="sec-pad shop-full shop-page">
    <div class="container"> 
        <div class="clearfix">
            <div class="pull-left">
                <div class="sec-title">
                    <div class="inner">
                        <span class="tag-line"><?php echo balanceTags($title);?></span>
                        <h2><?php echo balanceTags($title1);?></h2>
                        <span class="decor-line"></span>
                    </div><!-- /.inner-->
                </div><!-- /.sec-title-->
            </div><!-- /.pull-left -->
        </div><!-- /.clearfix -->       
        <div class="featured-product-carousel owl-theme owl-carousel">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$product_meta = _WSH()->get_meta();
			?>
            <div class="item">
                <div class="single-shop-item">
                    <div class="img-box">
                        <?php the_post_thumbnail('landshaper_270x270');?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="<?php echo esc_url(get_permalink(get_the_id()));?>"><i class="fa fa-link"></i></a>
                                        </li>
                                    </ul><!-- /.list-inlin -->
                                </div><!-- /.content -->
                            </div><!-- /.box -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                    <div class="text-box">
                        <h3><?php the_title();?></h3>
                        <p><?php woocommerce_template_loop_price(); ?></p>
                        <a href="<?php echo esc_url(get_permalink(get_the_id()));?>" class="thm-btn">Add to Cart</a>
                    </div><!-- /.text-box -->
                </div><!-- /.single-shop-item -->
            </div>
            <?php endwhile;?>
        </div><!-- /.featured-product-carousel -->        
    </div><!-- /.container -->
</section><!-- /.sec-pad shop-full shop-page -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>