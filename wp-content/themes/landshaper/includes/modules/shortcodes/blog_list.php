<?php  
   global $post ;
   $count = 0;
   $paged = get_query_var('paged');
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order, 'paged'=>$paged);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   


<?php while($query->have_posts()): $query->the_post();
	global $post ; 
	$post_meta = _WSH()->get_meta();
?>

<div class="single-news-post wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
	<div class="row">
		<div class="col-md-4">
			<div class="img-box">
				<?php the_post_thumbnail('landshaper_270x170');?>
				<div class="overlay">
					<div class="box">
						<div class="content">
							<a href="<?php echo esc_url(get_permalink(get_the_id()));?>" class="thm-btn"><?php esc_html_e('Read More', 'landshaper');?></a>
						</div><!-- /.content -->
					</div><!-- /.box -->
				</div><!-- /.overlay -->
			</div><!-- /.img-box -->
		</div><!-- /.col-md-4 -->
		<div class="col-md-8">
			<ul class="meta-info list-inline">
				<li>
					<a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php esc_html_e('by', 'landshaper');?> <?php the_author();?> </a>
				</li><!-- adding this comment for inline hack
				--><li>
					<a href="<?php echo esc_url(get_month_link(get_the_date('Y'), get_the_date('m'))); ?>"><?php echo get_the_date('M d, Y');?> </a>
				</li><!-- adding this comment for inline hack
				--><li>
					<?php the_tags('', ', ', ''); ?>
				</li><!-- adding this comment for inline hack
				--><li>
					<a href="<?php echo esc_url(get_permalink(get_the_id()).'#comment');?>"><?php comments_number( '0 comments', '1 comment', '% comments' ); ?></a>
				</li>
			</ul><!-- /.meta-info list-inline -->
			<h3><a href="<?php echo esc_url(get_permalink(get_the_id()));?>"><?php the_title();?></a></h3>
			<p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?></p>
		</div><!-- /.col-md-8 -->
	</div><!-- /.row -->
</div><!-- /.single-news-post -->
<?php endwhile;?>

<div class="post-nav-wrapper clearfix">
	<div class="pull-left">
		<div class="post-navigation list-inline ">
			<?php landshaper_the_pagination(array('total'=>$query->max_num_pages, 'next_text' => '<i class="fa fa-caret-right"></i>', 'prev_text' => '<i class="fa fa-caret-left"></i>')); ?>
		</div><!-- /.post-navigation -->
	</div><!-- /.pull-left -->
</div><!-- /.post-nav-wrapper -->


<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>