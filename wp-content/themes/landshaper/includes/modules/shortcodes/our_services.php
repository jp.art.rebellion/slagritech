<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<section class="sec-pad service-box-three">
    <div class="container">
        <div class="row">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$services_meta = _WSH()->get_meta();
			?>
            <div class="col-md-4">
                <div class="single-service-box-three">
                    <div class="img-box">
                        <?php the_post_thumbnail('landshaper_370x330');?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <div class="content-inner">
                                        <h3><?php the_title();?></h3>
                                        <span class="decor-line"></span>
                                        <p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?></p>
                                    </div><!-- /.content-inner -->
                                </div><!-- /.content -->
                            </div><!-- /.box -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                </div><!-- /.single-service-box-three -->
            </div><!-- /.col-md-4 -->
            <?php endwhile;?>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.sec-pad service-box-three -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>