<?php  
   global $post ;
   $count = 0;
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<section class="news-wrapper sec-pad">
    <div class="container">
        <div class="clearfix">
            <div class="pull-left">
                <div class="sec-title">
                    <div class="inner">
                        <span class="tag-line"><?php echo balanceTags($sub_title);?></span>
                        <h2><?php echo balanceTags($title);?></h2>
                        <span class="decor-line"></span>
                    </div><!-- /.inner-->
                </div><!-- /.sec-title-->
            </div><!-- /.pull-left -->
            <div class="pull-right">
                <a href="<?php echo esc_url($btn_link);?>" class="thm-btn"><?php echo balanceTags($btn_text);?></a>
            </div><!-- /.pull-right -->
        </div><!-- /.clearfix -->
        <div class="row">
            <?php while($query->have_posts()): $query->the_post();
                global $post ; 
                $post_meta = _WSH()->get_meta();
            ?>
            <div class="col-md-4">
                <div class="single-news-post wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="img-box">
                        <?php the_post_thumbnail('landshaper_370x200');?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url(get_permalink(get_the_id()));?>" class="thm-btn"><?php esc_html_e('Read More', 'landshaper');?></a>
                                </div><!-- /.content -->
                            </div><!-- /.box -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                    <ul class="meta-info list-inline">
                        <li>
                            <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php esc_html_e('by', 'landshaper');?> <?php the_author();?> </a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url(get_month_link(get_the_date('Y'), get_the_date('m'))); ?>"><?php echo get_the_date('M d, Y');?></a>
                        </li>
                    </ul><!-- /.meta-info list-inline -->
                    <h3><a href="<?php echo esc_url(get_permalink(get_the_id()));?>"><?php the_title();?></a></h3>
                </div><!-- /.single-news-post -->
            </div><!-- /.col-md-4 -->
            <?php endwhile;?>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.news-wrapper -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>