<?php
ob_start() ;?>

<section class="service-box-one-wrapper sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="left-text">
                    <div class="sec-title outer-stroke">
                        <div class="inner">
                            <span class="tag-line"><?php echo balanceTags($sub_title);?></span>
                            <h2><?php echo balanceTags($title);?></h2>
                            <span class="decor-line"></span>
                        </div><!-- /.inner-->
                    </div><!-- /.sec-title-->
                    <div class="text-box">
                        <p><?php echo balanceTags($text1);?></p>
                        <p><?php echo balanceTags($text2);?></p>
                        <a href="<?php echo esc_url($btn_link);?>" class="thm-btn"><?php echo balanceTags($btn_text);?></a>
                    </div><!-- /.text-box -->
                </div><!-- /.left-text -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6 wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
            	<img src="<?php echo esc_url(wp_get_attachment_url($bg_img));?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>)"/>
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.service-box-one-wrapper -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   