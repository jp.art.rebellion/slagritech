<?php 
landshaper_bunch_global_variable();
$paged = get_query_var('paged');
$args = array('post_type' => 'bunch_portfolio', 'showposts'=>$num, 'orderby'=>$sort, 'order'=>$order, 'paged'=>$paged);
$terms_array = explode(",",$exclude_cats);
if($exclude_cats) $args['tax_query'] = array(array('taxonomy' => 'portfolio_category','field' => 'id','terms' => $terms_array,'operator' => 'NOT IN',));
$query = new WP_Query($args);

$t = $GLOBALS['_bunch_base'];

$data_filtration = '';
$data_posts = '';
?>


<?php if( $query->have_posts() ):
	
ob_start();?>

	<?php $count = 0; 
	$fliteration = array();?>
	<?php while( $query->have_posts() ): $query->the_post();
		global  $post;
		$meta = get_post_meta( get_the_id(), '_bunch_portfolio_meta', true );//printr($meta);
		$meta1 = _WSH()->get_meta();
		$post_terms = get_the_terms( get_the_id(), 'portfolio_category');// printr($post_terms); exit();
		foreach( (array)$post_terms as $pos_term ) $fliteration[$pos_term->term_id] = $pos_term;
		$temp_category = get_the_term_list(get_the_id(), 'portfolio_category', '', ', ');
	?>
		<?php $post_terms = wp_get_post_terms( get_the_id(), 'portfolio_category'); 
		$term_slug = '';
		if( $post_terms ) foreach( $post_terms as $p_term ) $term_slug .= $p_term->slug.' ';?>		
           
		   <?php 
			$post_thumbnail_id = get_post_thumbnail_id($post->ID);
			$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
		   ?>     
		   <div class="col-md-3 filter-item <?php echo esc_attr($term_slug); ?>">
                <div class="single-project-item">
                    <div class="img-box">
                        <?php the_post_thumbnail('landshaper_270x200');?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <div class="top">
                                        <ul class="list-inline">
                                            <li>
                                                <a href="<?php echo esc_url(landshaper_set($meta1, 'ext_url'));?>"><i class="fa fa-link"></i></a>
                                            </li>
                                            <li>
                                                <a data-group="1" href="<?php echo esc_url($post_thumbnail_url);?>" class="img-popup"><i class="fa fa-search-plus"></i></a>
                                            </li>                                            
                                        </ul><!-- /.list-inline -->
                                    </div><!-- /.top -->
                                    <div class="bottom clearfix">
                                        <div class="title">
                                            <h3><?php the_title();?></h3>
                                        </div><!-- /.title -->
                                    </div><!-- /.bottom -->
                                </div><!-- /.content -->
                            </div><!-- /.box -->
                        </div><!-- /.overlay -->
                    </div><!-- /.img-box -->
                </div><!-- /.single-project-item -->
            </div><!-- /.col-md-3 -->
           
<?php endwhile;?>
  
<?php wp_reset_postdata();
$data_posts = ob_get_contents();
ob_end_clean();

endif; 

ob_start();?>	 

<?php $terms = get_terms(array('portfolio_category')); ?>

<section class="latest-project sec-pad <?php if($style_two == true) echo "full-width-gallery" ;?>">
    <div class="<?php if($style_two == true) echo "full-container" ;?> container">
        
        <div class="text-center">
            <ul class="post-filter light list-inline">
                <li class="active" data-filter=".filter-item"><span><?php esc_attr_e('All', 'landshaper');?></span></li>
                <?php foreach( $fliteration as $t ): ?>
                	<li data-filter=".<?php echo esc_attr(landshaper_set( $t, 'slug' )); ?>"><span><?php echo balanceTags(landshaper_set( $t, 'name')); ?></span></li>
                <?php endforeach;?>
            </ul><!-- /.gallery-filter -->
        </div><!-- /.text-center -->

        <div class="row masonary-layout filter-layout">
           <?php echo balanceTags($data_posts); ?>
        </div><!-- /.row -->

        <div class="post-navigation list-inline text-center">
			<?php landshaper_the_pagination(array('total'=>$query->max_num_pages, 'next_text' => '<i class="fa fa-caret-right"></i>', 'prev_text' => '<i class="fa fa-caret-left"></i>')); ?>
		</div><!-- /.post-navigation -->

    </div><!-- /.container -->
</section><!-- /.latest-project sec-pad -->

<?php $output = ob_get_contents();
ob_end_clean(); 
return $output;?>