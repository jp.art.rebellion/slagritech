<?php
ob_start() ;?>

<section class="fixed-banner sec-pad white-color-bg">
    <div class="background-slider" data-slides="[&quot;<?php echo esc_url(wp_get_attachment_url($img1));?>&quot;,&quot;<?php echo esc_url(wp_get_attachment_url($img2));?>&quot;,&quot;<?php echo esc_url(wp_get_attachment_url($img3));?>&quot;]"></div><!-- /.background-slider -->
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="banner-caption-box">
                    <div class="banner-caption-h2"><?php echo balanceTags($title1);?> <br /><?php echo balanceTags($title2);?></div>
                    <div class="banner-caption-p"><?php echo balanceTags($text1);?><br /><?php echo balanceTags($text2);?></div>
                    <a href="<?php echo esc_url($btn_link1);?>" class="thm-btn"><?php echo balanceTags($btn_text1);?></a>&emsp;&emsp;<a href="<?php echo esc_url($btn_link2);?>" class="thm-btn borderd inverse"><?php echo balanceTags($btn_text2);?></a>
                </div>
            </div><!-- /.col-md-8 -->
            <div class="col-md-4">
                <div class="request-qoute-box white-color-bg ">                    
                    <div class="inner-box">
                        <div class="sec-title medium outer-stroke">
                            <div class="inner">
                                <h2 class="base-color-text"><?php echo balanceTags($title3);?></h2>
                                <span class="decor-line"></span>
                            </div><!-- /.inner-->
                        </div><!-- /.sec-title-->
                        <?php echo do_shortcode(bunch_base_decode($slider_form));?>        
                    </div><!-- /.inner-box -->
                </div><!-- /.request-qoute-box -->
            </div><!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.fixed-banner -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   