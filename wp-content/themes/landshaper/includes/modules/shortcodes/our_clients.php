<?php  
ob_start() ;
$options = _WSH()->option();
?>

<section class="base-color-bg client-carousel-wrapper">
    <div class="container">
        <div class="client-carousel owl-theme owl-carousel">
            
			<?php if($clients = landshaper_set(landshaper_set($options, 'clients'), 'clients')):?>
			<?php foreach($clients as $key => $value):?>
				<?php if(landshaper_set($value, 'tocopy')) continue;?>
                <div class="item"><a href="<?php echo esc_url(landshaper_set($value, 'client_link'));?>"><img src="<?php echo esc_url(landshaper_set($value, 'client_img'));?>" alt="<?php esc_html_e('image', 'landshaper');?>" title="<?php echo esc_attr(landshaper_set($value, 'title'));?>"></a></div>
                <?php endforeach;?>
        	<?php endif;?>
        
        </div><!-- /.client-carusel owl-theme owl-carousel -->
    </div><!-- /.container -->
</section><!-- /.base-color-bg -->

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   