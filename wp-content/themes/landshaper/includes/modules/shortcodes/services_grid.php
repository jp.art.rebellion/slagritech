<?php  
   $count = 0;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ob_start() ;?>
<?php if($query->have_posts()):  ?>   

<section class="service-box-one-wrapper service-page sec-pad">
    <div class="container">
        
        <div class="single-service-box-wrapper">
	        <div class="row">
	            <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$services_meta = _WSH()->get_meta();
				?>
                <?php if(($count%3) == 0 && $count != 0):?>
                </div></div><div class="single-service-box-wrapper"><div class="row">
                <?php endif;?>
                <div class="col-md-4">
	                <div class="single-service-box-one">
	                    <div class="img-box">
	                        <?php the_post_thumbnail('landshaper_370x150');?>
	                        <div class="overlay">
	                            <div class="box">
	                                <div class="content">
	                                    <a href="<?php echo esc_url(landshaper_set($services_meta, 'ext_url'))?>" class="thm-btn"><?php esc_html_e('Read More', 'landshaper');?></a>
	                                </div><!-- /.content -->
	                            </div><!-- /.box -->
	                        </div><!-- /.overlay -->
	                    </div><!-- /.img-box -->
	                    <h3><?php the_title();?></h3>
	                    <p><?php echo balanceTags(landshaper_trim(get_the_content(), $text_limit));?></p>
	                </div><!-- /.single-service-box-one -->
	            </div><!-- /.col-md-4 -->
	        	<?php $count++; endwhile; ?>
            </div><!-- /.row -->
        </div><!-- /.single-service-box-wrapper -->
    </div><!-- /.container -->
</section><!-- /.service-box-one-wrapper -->

<?php endif; ?>
<?php 
	wp_reset_postdata();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>