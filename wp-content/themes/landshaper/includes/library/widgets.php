<?php
///----Blog widgets---

//About Us
class Bunch_About_Info extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Abous_Info', /* Name */esc_html__('Landshaper About Information','landshaper'), array( 'description' => esc_html__('Show the information about company', 'landshaper' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo balanceTags($before_widget);?>
      		
			<div class="wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                <div class="about-widget">
                    <?php echo balanceTags($before_title.$title.$after_title); ?>
                    
                    <img src="<?php echo esc_url($instance['img']);?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>"/>
                    <p><?php echo balanceTags($instance['content']); ?></p>
                    <a href="<?php echo esc_url($instance['link']); ?>" class="read-more"><?php esc_html_e('Read More', 'landshaper');?> <i class="fa fa-caret-right"></i></a>
                </div><!-- /.about-widget -->
            </div>
            
        <?php
		
		echo balanceTags($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['img'] = strip_tags($new_instance['img']);
		$instance['content'] = $new_instance['content'];
		$instance['link'] = $new_instance['link'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : esc_html__('About Us', 'landshaper');
		$img = ($instance) ? esc_attr($instance['img']) : 'http://asianitbd.com/wp/landshaper/wp-content/themes/landshaper/img/resources/sidebar-about.jpg';
		$content = ($instance) ? esc_attr($instance['content']) : '';
		$link = ( $instance ) ? esc_attr($instance['link']) : '#';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Title here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('img')); ?>"><?php esc_html_e('Image Link:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Image link here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('img')); ?>" name="<?php echo esc_attr($this->get_field_name('img')); ?>" type="text" value="<?php echo esc_attr($img); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('Content:', 'landshaper'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo balanceTags($content); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('link')); ?>"><?php esc_html_e('Link:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Link here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('link')); ?>" name="<?php echo esc_attr($this->get_field_name('link')); ?>" type="text" value="<?php echo esc_attr($link); ?>" />
        </p>       
                
		<?php 
	}
	
}

/// Recent Posts 
class Bunch_Recent_Post extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Recent_Post', /* Name */esc_html__('Landshaper Recent Posts','landshaper'), array( 'description' => esc_html__('Show the recent posts', 'landshaper' )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo balanceTags($before_widget); ?>
		
        <!-- Recent Posts -->
        <div class="wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <div class="latest-news">
                
				<?php echo balanceTags($before_title.$title.$after_title); ?>
                
                <?php $query_string = 'posts_per_page='.$instance['number'];
					if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
					
					$this->posts($query_string);
				?>
                                     
                <a href="<?php echo esc_url($instance['post_link']);?>" class="view-all"><i class="fa fa-plus"></i> <?php esc_html_e('View All', 'landshaper');?></a>
            </div><!-- /.latest-news -->
        </div>
        
		<?php echo balanceTags($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		$instance['post_link'] = $new_instance['post_link'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : esc_html__(' Popular News', 'landshaper');
		$number = ( $instance ) ? esc_attr($instance['number']) : 3;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title: ', 'landshaper'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('No. of Posts:', 'landshaper'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
       
    	<p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'landshaper'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'landshaper'), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
            
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('post_link')); ?>"><?php esc_html_e('All Post Link: ', 'landshaper'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('post_link')); ?>" name="<?php echo esc_attr($this->get_field_name('post_link')); ?>" type="text" value="<?php echo esc_attr( $post_link ); ?>" />
        </p>
        
		<?php 
	}
	
	function posts($query_string)
	{
		$query = new WP_Query($query_string) ;
		if( $query->have_posts() ):?>
        
           	<!-- Title -->
				
            <ul class="latest-post">
            	<?php while( $query->have_posts() ): $query->the_post(); ?>
                <li>
                    <div class="icon-box">
                        <?php the_post_thumbnail('landshaper_75x75');?>
                        <div class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="<?php echo esc_url(get_permalink(get_the_id()));?>">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div><!-- /.content -->
                            </div><!-- /.box -->
                        </div><!-- /.overlay -->
                    </div><!-- /.icon-box -->
                    <div class="text-box">
                        <a href="<?php echo esc_url(get_permalink(get_the_id()));?>"><h4><?php echo balanceTags(landshaper_trim(get_the_title(), '4'));?></h4></a>
                        <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><p><?php esc_html_e('by', 'landshaper');?> <?php the_author();?></p></a>
                    </div><!-- /.text-box -->
                </li>
                <?php endwhile; ?>
            </ul>
            
        <?php endif;
		wp_reset_postdata();
    }
}

///----footer widgets---
//About Us
class Bunch_About_us extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Abous_us', /* Name */esc_html__('Landshaper About Us','landshaper'), array( 'description' => esc_html__('Show the information about company', 'landshaper' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		
		echo balanceTags($before_widget);?>
      		
			<div class="about-widget">
                <a href="<?php echo esc_url(home_url('/'));?>"><img src="<?php echo esc_url($instance['logo_url']);?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>" /></a>
                <p><?php echo balanceTags($instance['content']); ?></p> 
                <a href="<?php echo esc_url($instance['purchase_link']); ?>" class="thm-btn"><?php echo balanceTags($instance['purchase_text']); ?></a>
            </div>
            
		<?php
		
		echo balanceTags($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['logo_url'] = strip_tags($new_instance['logo_url']);
		$instance['content'] = $new_instance['content'];
		$instance['purchase_link'] = $new_instance['purchase_link'];
		$instance['purchase_text'] = $new_instance['purchase_text'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$logo_url = ($instance) ? esc_attr($instance['logo_url']) : 'http://asianitbd.com/wp/landshaper/wp-content/themes/landshaper/img/logo2.png';
		$content = ($instance) ? esc_attr($instance['content']) : '';
		$purchase_link = ( $instance ) ? esc_attr($instance['purchase_link']) : '#';
		$purchase_text = ( $instance ) ? esc_attr($instance['purchase_text']) : 'Purchase for 17$';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('logo_url')); ?>"><?php esc_html_e('Logo:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Logo link here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('logo_url')); ?>" name="<?php echo esc_attr($this->get_field_name('logo_url')); ?>" type="text" value="<?php echo esc_attr($logo_url); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('Content:', 'landshaper'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo balanceTags($content); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('purchase_link')); ?>"><?php esc_html_e('Purchase Link:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Purchase Link here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('purchase_link')); ?>" name="<?php echo esc_attr($this->get_field_name('purchase_link')); ?>" type="text" value="<?php echo esc_attr($purchase_link); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('purchase_text')); ?>"><?php esc_html_e('Purchase Text:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Logo Text here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('purchase_text')); ?>" name="<?php echo esc_attr($this->get_field_name('purchase_text')); ?>" type="text" value="<?php echo esc_attr($purchase_text); ?>" />
        </p>        
                
		<?php 
	}
	
}

//About Us
class Bunch_feed_burner extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_feed_burner', /* Name */esc_html__('Feed Burner Mailing Address','landshaper'), array( 'description' => esc_html__('Show the Mailing Address', 'landshaper' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo balanceTags($before_widget);?>
      		
			<div class="subscribe-widget">
                <?php echo balanceTags($before_title.$title.$after_title); ?>
                
                <p class="highlight"><?php echo balanceTags($instance['content']); ?></p>
                <form action="http://feedburner.google.com/fb/a/mailverify" accept-charset="utf-8" class="clearfix mailchimp-form">
                    <input type="text" name="email" placeholder="<?php esc_html_e('Email address', 'landshaper');?>" />
                    <input type="hidden" id="uri2" name="uri" value="<?php echo balanceTags($id); ?>">
                    <button type="submit"><i class="fa fa-envelope-o"></i></button>
                </form>
                <p><?php esc_html_e('Get Latest Updates & Offer', 'landshaper');?></p>
                <div class="result"></div><!-- /.result -->
                <?php if( $instance['show'] ): ?>
                <ul class="list-inline social">
                	<?php echo balanceTags(landshaper_get_social_icons()); ?>
                </ul><!-- /.list-inline -->
                <?php endif;?>
            </div>
            
		<?php
		
		echo balanceTags($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['content'] = $new_instance['content'];
		$instance['id'] = $new_instance['id'];
		$instance['show'] = $new_instance['show'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : 'Subscribe us';
		$content = ($instance) ? esc_attr($instance['content']) : '';
		$id = ( $instance ) ? esc_attr($instance['id']) : '#';
		$show = ( $instance ) ? esc_attr($instance['show']) : '';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Title here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('Content:', 'landshaper'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo balanceTags($content); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('id')); ?>"><?php esc_html_e('ID:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Id here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('id')); ?>" name="<?php echo esc_attr($this->get_field_name('id')); ?>" type="text" value="<?php echo esc_attr($id); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('show')); ?>"><?php esc_html_e('Show Social Icons:', 'landshaper'); ?></label>
			<?php $selected = ( $show ) ? ' checked="checked"' : ''; ?>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('show')); ?>"<?php echo esc_attr($selected); ?> name="<?php echo esc_attr($this->get_field_name('show')); ?>" type="checkbox" value="true" />
        </p>        
                
		<?php 
	}
	
}

//About Us
class Bunch_Contact_Info extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Contact_Info', /* Name */esc_html__('Landshaper Contact Us','landshaper'), array( 'description' => esc_html__('Show the Contact information about company', 'landshaper' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo balanceTags($before_widget);?>
      		
			<div class="footer-widget contact-widget">
                <?php echo balanceTags($before_title.$title.$after_title); ?>
                <div class="footer-contact-info-carousel">
                    <div class="item">
                        <ul class="contact-infos">
                            <li>
                                <div class="icon-box">
                                    <i class="lsp-icon-signs"></i>
                                </div><!-- /.icon-box -->
                                <div class="text-box">
                                    <p><?php echo nl2br(wp_kses_post($instance['address'])); ?></p>
                                </div><!-- /.text-box -->
                            </li>
                            <li>
                                <div class="icon-box">
                                    <i class="lsp-icon-technology"></i>
                                </div><!-- /.icon-box -->
                                <div class="text-box">
                                    <p><?php esc_html_e('Call us:', 'landshaper');?> <?php echo nl2br(wp_kses_post($instance['phone'])); ?></p>
                                </div><!-- /.text-box -->
                            </li>
                            <li>
                                <div class="icon-box">
                                    <i class="lsp-icon-note"></i>
                                </div><!-- /.icon-box -->
                                <div class="text-box">
                                    <p><?php esc_html_e('Mail us :', 'landshaper');?> <?php echo balanceTags($instance['email']); ?> </p>
                                </div><!-- /.text-box -->
                            </li>
                            <li>
                                <div class="icon-box">
                                    <i class="lsp-icon-clock"></i>
                                </div><!-- /.icon-box -->
                                <div class="text-box">
                                    <p><?php echo balanceTags($instance['opening_schdule']); ?></p>
                                </div><!-- /.text-box -->
                            </li>
                        </ul><!-- /.contact-infos -->						
                    </div>
                </div><!-- /.contact-info-carousel owl-theme owl-carousel -->
                
            </div>
            
		<?php
		
		echo balanceTags($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = $new_instance['phone'];
		$instance['email'] = $new_instance['email'];
		$instance['opening_schdule'] = $new_instance['opening_schdule'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : '';
		$address = ($instance) ? esc_attr($instance['address']) : '';
		$phone = ($instance) ? esc_attr($instance['phone']) : '';
		$email = ( $instance ) ? esc_attr($instance['email']) : '';
		$opening_schdule = ( $instance ) ? esc_attr($instance['opening_schdule']) : '';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Title here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php esc_html_e('Address:', 'landshaper'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('address')); ?>" name="<?php echo esc_attr($this->get_field_name('address')); ?>" ><?php echo nl2br(wp_kses_post($address)); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php esc_html_e('Phone No:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Phone No', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php esc_html_e('Email Address:', 'landshaper'); ?></label>
            <input placeholder="<?php esc_html_e('Email Address here', 'landshaper');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('opening_schdule')); ?>"><?php esc_html_e('Opening Schdule:', 'landshaper'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('opening_schdule')); ?>" name="<?php echo esc_attr($this->get_field_name('opening_schdule')); ?>" ><?php echo balanceTags($opening_schdule); ?></textarea>
        </p>       
                
		<?php 
	}
	
}

/// Externel Links
class Bunch_Services_Link extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Services_Link', /* Name */esc_html__('Landshaper Services Link','landshaper'), array( 'description' => esc_html__('Show the Services Links', 'landshaper' )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo balanceTags($before_widget); ?>
		
        <!-- Recent Posts -->
        <div class="service-list-widget">
            <ul class="service-list">
                <?php 
					$args = array('post_type' => 'bunch_services', 'showposts'=>$instance['number']);
					if( $instance['cat'] ) $args['tax_query'] = array(array('taxonomy' => 'services_category','field' => 'id','terms' => (array)$instance['cat']));
					
					$this->posts($args);
				?>
            </ul><!-- /.service-list -->	
        </div>
        
		<?php echo balanceTags($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$number = ( $instance ) ? esc_attr($instance['number']) : 7;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('No. of Posts:', 'landshaper'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
       
    	<p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'landshaper'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'landshaper'), 'selected'=>$cat, 'taxonomy' => 'services_category', 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
        
		<?php 
	}
	
	function posts($args)
	{
		$query = new WP_Query($args) ;
		if( $query->have_posts() ):?>
        
           	<!-- Title -->
				
            <?php while( $query->have_posts() ): $query->the_post();
			 $services_meta = _WSH()->get_meta();
			 ?>
                <li class=""><a href="<?php echo esc_url(landshaper_set($services_meta, 'ext_url'));?>"><?php the_title();?></a></li>
            <?php endwhile; ?>
            
            
        <?php endif;
		wp_reset_postdata();
    }
}

//Agent Info
class Bunch_Agent_Info extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Agent_Info', /* Name */esc_html__('Landshaper Agent Info','landshaper'), array( 'description' => esc_html__('Show the Agent Info', 'landshaper' )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo balanceTags($before_widget); ?>
		
        <div class="single-sidebar">
            <div class="agent-widget">
                <?php 
				$args = array('post_type' => 'bunch_testimonials', 'showposts'=>$instance['number']);
				if( $instance['cat'] ) $args['tax_query'] = array(array('taxonomy' => 'testimonials_category','field' => 'id','terms' => (array)$instance['cat']));
					
				$this->posts($args);
			?>
            </div><!-- /.agent-widget -->
        </div>
        
		<?php echo balanceTags($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$number = ( $instance ) ? esc_attr($instance['number']) : 2;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php esc_html_e('No. of Posts:', 'landshaper'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
       
    	<p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php esc_html_e('Category', 'landshaper'); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>esc_html__('All Categories', 'landshaper'), 'selected'=>$cat, 'taxonomy' => 'testimonials_category', 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
        
		<?php 
	}
	
	function posts($args)
	{
		$query = new WP_Query($args) ;
		if( $query->have_posts() ):?>
        
           	<!-- Title -->
				
            <?php while( $query->have_posts() ): $query->the_post();
			 $services_meta = _WSH()->get_meta();
			 ?>
                
                <div class="single-agent-widget">
                    <h3><?php the_title();?></h3>
                    <div class="info-box">
                        <div class="icon-box">
                        	<?php the_post_thumbnail('landshaper_67x64');?>
                        </div><!-- /.icon-box -->
                        <div class="text-box">
                            <h4><?php echo wp_kses_post(landshaper_set($services_meta, 'designation'));?></h4>
                            <ul>
                                <li><i class="fa fa-phone"></i> <?php echo nl2br(wp_kses_post(landshaper_set($services_meta, 'phone')));?></li>
                                <li><i class="fa fa-envelope"></i> <?php echo sanitize_email(landshaper_set($services_meta, 'email'));?></li>
                            </ul>
                        </div><!-- /.text-box -->
                    </div><!-- /.info-box -->
                </div><!-- /.single-agent-widget -->
            <?php endwhile; ?>
            
            
        <?php endif;
		wp_reset_postdata();
    }
}
