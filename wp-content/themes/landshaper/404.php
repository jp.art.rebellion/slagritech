<?php
$options = _WSH()->option();
    get_header(); 
?>

<div class="has-base-color-overlay sec-pad text-center error-404">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
			<div class="top-box">
				<h1><?php esc_html_e('404', 'landshaper');?></h1>
				<h2><?php esc_html_e('Opps! You have some problems', 'landshaper');?></h2>
			</div><!-- /.top-box -->
			<p><?php esc_html_e('The page you are looking for was moved, removed, renamed or ', 'landshaper');?><br /><?php esc_html_e('might never existed.', 'landshaper');?></p>
			<a href="<?php echo esc_url(home_url('/')); ?>" class="thm-btn inverse"><?php esc_html_e('Back To Home', 'landshaper');?></a>
		</div>
	</div><!-- /.container -->
</div>
 		
<?php get_footer(); ?>