<?php landshaper_bunch_global_variable(); 
	$options = _WSH()->option();
	get_header(); 
	if( $wp_query->is_posts_page ) {
		$meta = _WSH()->get_meta('_bunch_layout_settings', get_queried_object()->ID);
		$meta1 = _WSH()->get_meta('_bunch_header_settings', get_queried_object()->ID);
		if(landshaper_set($_GET, 'layout_style')) $layout = landshaper_set($_GET, 'layout_style'); else
		$layout = landshaper_set( $meta, 'layout', 'right' );
		$sidebar = landshaper_set( $meta, 'sidebar', 'default-sidebar' );
		$bg = landshaper_set($meta1, 'header_img');
		$title = landshaper_set($meta1, 'header_title');
		$link = landshaper_set($meta1, 'header_link');
	} else {
		$settings  = _WSH()->option(); 
		if(landshaper_set($_GET, 'layout_style')) $layout = landshaper_set($_GET, 'layout_style'); else
		$layout = landshaper_set( $settings, 'archive_page_layout', 'right' );
		$sidebar = landshaper_set( $settings, 'archive_page_sidebar', 'default-sidebar' );
		$bg = landshaper_set($settings, 'archive_page_header_img');
		$title = landshaper_set($settings, 'archive_page_header_title');
		$link = landshaper_set($settings, 'archive_page_header_link');
	}
	$layout = landshaper_set( $_GET, 'layout' ) ? landshaper_set( $_GET, 'layout' ) : $layout;
	$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
	_WSH()->page_settings = array('layout'=>'right', 'sidebar'=>$sidebar);
	$classes = ( !$layout || $layout == 'full' || landshaper_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-md-9 col-sm-12 col-xs-12 ' ;
	?>
	
    <!--Page Title-->
    <div class="inner-banner has-base-color-overlay text-center" <?php if($bg):?>style="background-image:url('<?php echo esc_attr($bg)?>');"<?php endif;?>>
        <div class="container">
            <div class="box">
                <h3><?php if($title) echo wp_kses_post($title); else esc_html_e('Blog', 'landshaper');?></h3>
            </div><!-- /.box -->
        </div><!-- /.container -->
    </div><!-- /.inner-banner -->
    
    <div class="breadcumb-wrapper">
        <div class="container">
            <div class="pull-left">
                <?php echo balanceTags(landshaper_get_the_breadcrumb()); ?>
            </div><!-- /.pull-left -->
        </div><!-- /.container -->
    </div><!-- /.breadcumb-wrapper -->
    
    <!--Sidebar Page-->
    <section class="page sidebar-page sec-pad news-wrapper news-grid-sidebar-page">
        <div class="container">        
            <div class="row">
                
                <!-- sidebar area -->
                <?php if( $layout == 'left' ): ?>
                <?php if ( is_active_sidebar( $sidebar ) ) { ?>
                <div class="col-md-3 col-sm-12 col-xs-12">        
                    <?php dynamic_sidebar( $sidebar ); ?>
                </div>
                <?php } ?>
                <?php endif; ?>
                
                <!--Content Side-->	
                <div class="<?php echo esc_attr($classes);?>">
                    <!--Blog Post-->
					<?php while( have_posts() ): the_post();?>
                        <!-- blog post item -->
                        <!-- Post -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class();?>>
                            <?php get_template_part( 'blog' ); ?>
                        <!-- blog post item -->
                        </div><!-- End Post -->
                    <?php endwhile;?>
                    
                    <!-- post-navigation -->
                    <div class="post-nav-wrapper clearfix">
                        <div class="pull-left">
                            <div class="post-navigation list-inline ">
                                <?php landshaper_the_pagination(); ?>
                            </div><!-- /.post-navigation -->
                        </div><!-- /.pull-left -->
                    </div><!-- /.post-nav-wrapper -->
                </div>
                <!--Content Side-->
                
                <!--Sidebar-->	
                <!-- sidebar area -->
                <?php if( $layout == 'right' ): ?>
                <?php if ( is_active_sidebar( $sidebar ) ) { ?>
                <div class="col-md-3 col-sm-12 col-xs-12">        
                    <?php dynamic_sidebar( $sidebar ); ?>
                </div>
                <?php } ?>
                <?php endif; ?>
                <!--Sidebar-->
            </div>
        </div>
    </section>
<?php get_footer(); ?>