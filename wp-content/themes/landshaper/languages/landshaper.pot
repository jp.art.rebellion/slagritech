msgid ""
msgstr ""
"Project-Id-Version: Landshaper version 1.0\n"
"POT-Creation-Date: 2017-02-06 17:17+0500\n"
"PO-Revision-Date: 2017-02-07 19:15+0500\n"
"Last-Translator: amir <amirkhan0617@gmail.com>\n"
"Language-Team: theme kalia <muhibbur@gmail.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.9\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: esc_html__;esc_html_e;_n;_x\n"
"X-Poedit-SearchPath-0: D:\\xampp\\htdocs\\test_work\\landshaper\\wp-content"
"\\themes\\landshaper\n"

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/404.php:10
msgid "404"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/404.php:11
msgid "Opps! You have some problems"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/404.php:13
msgid "The page you are looking for was moved, removed, renamed or "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/404.php:13
msgid "might never existed."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/404.php:14
msgid "Contact Us"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/archive.php:31
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/author.php:31
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/category.php:30
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/index.php:43
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/page.php:30
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/search.php:30
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/single.php:32
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/tag.php:31
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/tpl-visual_composer.php:24
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/archive-product.php:49
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/single-product.php:46
msgid "Make an appointment"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/blog.php:10
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:28
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/blog_grid.php:25
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/blog_list.php:25
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/latest_news.php:38
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/services_grid.php:28
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/sidebar.php:35
msgid "Read More"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/blog.php:18
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:187
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/blog_grid.php:32
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/blog_list.php:34
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/latest_news.php:45
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/single.php:66
msgid "by"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/comments.php:37
msgid "Comment navigation"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/comments.php:38
msgid "&larr; Older Comments"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/comments.php:39
msgid "Newer Comments &rarr;"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/comments.php:43
msgid "Comments are closed."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/comments.php:52
msgid "Leave a"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/comments.php:52
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:252
msgid "Reply"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:34
msgid "Main Menu"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:68
msgid "Default Sidebar"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:70
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:89
msgid "Widgets in this area will be shown on the right-hand side."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:77
msgid "Footer Sidebar"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:79
msgid "Widgets in this area will be shown in Footer Area."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:87
msgid "Blog Listing"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:216
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:217
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:218
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/functions.php:224
msgid "on"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:25
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:152
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:533
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:616
msgid "All Categories"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:49
msgid "No Sidebar"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:116
msgid "Home"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:164
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:165
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:166
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:167
msgid "Archive for "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:168
msgid "Search Results for "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:169
msgid "404 - Not Found"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:187
msgid "Search results for &ldquo;"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:214
msgid "You must agreed the policy"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:224
msgid "Registration Successful - An email is sent"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:228
msgid "Username or email already exists.  Password inherited."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:241
msgid "commenter"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:249
msgid "Posted on"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:282
msgid "Your Name"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:283
msgid "Enter Your Email Address"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:284
msgid "Enter a Subject"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:286
#, php-format
msgid "Required fields are marked %s"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:298
msgid "Type Message Here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:301
msgid "Your email address will not be published."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:306
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:307
msgid "Cancel reply"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:308
msgid "Submit Comment "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:590
msgid "Poor"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:590
msgid "Satisfactory"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:590
msgid "Good"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:590
msgid "Better"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:590
msgid "Awesome"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:612
#, php-format
msgid "Average Rating %s"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:652
msgid "Loading..."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/functions.php:701
msgid "logo"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:11
msgid "Landshaper About Information"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:11
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:205
msgid "Show the information about company"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:26
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:217
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v1.php:63
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v1.php:65
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v4.php:11
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v4.php:13
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v5.php:12
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v5.php:14
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/about_us.php:24
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/electric_mover.php:11
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/electric_mover.php:34
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/electric_mover.php:49
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/our_history.php:27
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/request_an_estimate_2.php:9
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/service_detail_projects.php:7
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/service_detail_projects.php:17
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/service_detail_projects.php:45
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/video_section.php:20
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/video_section.php:25
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/video_section.php:29
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/welcome_our_company.php:32
msgid "Awesome Image"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:54
msgid "About Us"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:60
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:335
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:448
msgid "Title:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:61
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:336
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:449
msgid "Title here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:64
msgid "Image Link:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:65
msgid "Image link here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:68
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:254
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:339
msgid "Content:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:72
msgid "Link:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:73
msgid "Link here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:87
msgid "Landshaper Recent Posts"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:87
msgid "Show the recent posts"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:113
msgid "View All"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:137
msgid " Popular News"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:142
msgid "Title: "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:146
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:527
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:610
msgid "No. of Posts:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:151
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:532
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:615
msgid "Category"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:156
msgid "All Post Link: "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:205
msgid "Landshaper About Us"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:250
msgid "Logo:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:251
msgid "Logo link here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:258
msgid "Purchase Link:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:259
msgid "Purchase Link here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:262
msgid "Purchase Text:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:263
msgid "Logo Text here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:278
msgid "Feed Burner Mailing Address"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:278
msgid "Show the Mailing Address"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:294
msgid "Email address"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:298
msgid "Get Latest Updates & Offer"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:343
msgid "ID:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:344
msgid "Id here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:347
msgid "Show Social Icons:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:364
msgid "Landshaper Contact Us"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:364
msgid "Show the Contact information about company"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:393
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/contact_us.php:26
msgid "Call us:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:401
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/contact_us.php:34
msgid "Mail us :"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:452
msgid "Address:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:456
msgid "Phone No:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:457
msgid "Phone No"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:460
msgid "Email Address:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:461
msgid "Email Address here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:464
msgid "Opening Schdule:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:479
msgid "Landshaper Services Link"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:479
msgid "Show the Services Links"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:563
msgid "Landshaper Agent Info"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/library/widgets.php:563
msgid "Show the Agent Info"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/loader.php:146
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/loader.php:147
msgid "Theme Options"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/loader.php:150
msgid "Facebook"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/loader.php:150
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/single.php:89
msgid "Twitter"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/loader.php:150
msgid "Dribble"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/loader.php:150
msgid "Github"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/loader.php:151
msgid "Flickr"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/loader.php:151
msgid "Google+"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/loader.php:151
msgid "Youtube"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v1.php:33
msgid "Troll Free :"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v1.php:56
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v2.php:56
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v3.php:55
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v4.php:48
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v5.php:32
msgid "Toggle navigation"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v1.php:97
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v3.php:85
msgid "Search"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v2.php:28
msgid "Careers"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v2.php:30
msgid "Helpdesk"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v2.php:33
msgid "Support"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v2.php:62
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v2.php:64
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v3.php:61
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v3.php:63
msgid "Logo"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v2.php:84
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v4.php:23
msgid "Get a Quote"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v3.php:88
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/searchform2.php:2
msgid "Search Here"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v4.php:18
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/searchform3.php:2
msgid "Search..."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v4.php:88
msgid "Cart"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/header_v5.php:19
msgid "Call Us:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/appoinment_form.php:22
msgid "Your Name *"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/appoinment_form.php:27
msgid "Your Mail * "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/appoinment_form.php:32
msgid "Phone Number"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/appoinment_form.php:37
msgid "Address"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/appoinment_form.php:42
msgid "Date of Appoinment"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/appoinment_form.php:47
msgid "Time of Appoinment"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/appoinment_form.php:52
msgid "Your Message"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/appoinment_form.php:60
msgid "Select Your Service:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/appoinment_form.php:70
msgid "Make an Appoinment"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/customer_feedback.php:54
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/testimonial_feedback.php:66
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/testimonial_slider.php:26
msgid "-"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/our_clients.php:13
msgid "image"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/request_an_estimate.php:29
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/request_an_estimate_2.php:25
msgid "For Business :"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/request_an_estimate.php:30
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/request_an_estimate_2.php:26
msgid "Office Hours :"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/video_section.php:11
msgid "“"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/modules/shortcodes/video_section.php:13
msgid "”"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:608
msgid "Update Required"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:912
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:2618
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:3665
msgid "Return to the Dashboard"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:1019
msgid ""
"The remote plugin package does not contain a folder with the desired slug "
"and renaming did not work."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:1019
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:1022
msgid ""
"Please contact the plugin provider and ask them to package their plugin "
"according to the WordPress guidelines."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:1022
msgid ""
"The remote plugin package consists of more than one file, but the files are "
"not packaged in a folder."
msgstr ""

#. translators: 1: install status, 2: update status
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:2427
#, php-format
msgid "%1$s, %2$s"
msgstr ""

#. translators: 1: number of plugins.
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:2477
#, php-format
msgid "To Install <span class=\"count\">(%s)</span>"
msgstr ""

#. translators: 1: number of plugins.
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:2481
#, php-format
msgid "Update Available <span class=\"count\">(%s)</span>"
msgstr ""

#. translators: 1: number of plugins.
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:2485
#, php-format
msgid "To Activate <span class=\"count\">(%s)</span>"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:2567
msgid "unknown"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:2618
msgid "No plugins to install, update or activate."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:2769
msgid "Upgrade message from the plugin author:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:2993
msgid "No plugins were selected to be activated. No action taken."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:3019
msgid "No plugins are available to be activated at this time."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:3037
msgid "The following plugin was activated successfully:"
msgstr ""

#. translators: 1: plugin name.
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:3594
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:3602
msgid "Show Details"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:3594
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:3602
msgid "Hide Details"
msgstr ""

#. translators: 1: plugin name.
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php:3602
#, php-format
msgid "%1$s installed successfully."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/plugins.php:63
msgid "Theme Support (Required)"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/plugins.php:74
msgid "Revolution Slider"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/plugins.php:85
msgid "WPBakery Visual Composer"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/plugins.php:96
msgid "Contact Form 7"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/thirdparty/tgm-plugin-activation/plugins.php:101
msgid "Woocommerce"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/vc_map.php:6
msgid "bbPress Forums"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/vc_map.php:9
msgid "Jollyall"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/vc_map.php:16
msgid "Forum"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/vc_map.php:179
msgid "Container"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/includes/vc_map.php:182
msgid "Choose whether you want to add a container before row or not."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/search.php:73
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/searchform.php:4
msgid "Enter your keyword"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/sidebar.php:15
msgid "by : "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/sidebar.php:17
msgid "in : "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/sidebar.php:19
msgid " 123 Likes "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/sidebar.php:21
msgid " Print "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/single.php:80
msgid "Pages: "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/single.php:85
msgid "Did You Like This Post?"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/single.php:88
msgid "facebook"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/single.php:90
msgid "Google Plus"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/single.php:91
msgid "Pinterest"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart-totals.php:19
msgid "Cart Totals"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart-totals.php:24
msgid "Subtotal"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart-totals.php:61
#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart.php:30
msgid "Total"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart.php:26
msgid "Preview"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart.php:27
msgid "Product"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart.php:28
msgid "Price"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart.php:29
msgid "Quantity"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart.php:71
msgid "Available on backorder"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart.php:110
msgid "Remove this item"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/cart/cart.php:165
msgid "Shipping"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/checkout/form-checkout.php:20
msgid "You must be logged in to checkout."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/checkout/form-checkout.php:45
msgid "Your order"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/checkout/payment.php:34
msgid ""
"Sorry, it seems that there are no available payment methods for your state. "
"Please contact us if you require assistance or wish to make alternate "
"arrangements."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/checkout/payment.php:34
msgid "Please fill in your details above to see available payment methods."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/checkout/payment.php:41
msgid ""
"Since your browser does not support JavaScript, or it is disabled, please "
"ensure you click the <em>Update Totals</em> button before placing your "
"order. You may be charged more than the amount stated above if you fail to "
"do so."
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/content-product.php:92
msgid "Add to Cart"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/content-single-product.php:83
msgid "Product Overview:"
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/single-product/related.php:42
msgid "Related "
msgstr ""

#: D:\xampp\htdocs\test_work\landshaper\wp-content\themes\landshaper/woocommerce/single-product/related.php:42
msgid "Products"
msgstr ""
