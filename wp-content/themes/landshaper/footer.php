<?php $options = get_option('wp_landshaper'.'_theme_options');?>
	<?php if ( is_active_sidebar( 'footer-sidebar' ) ) { ?>
    <footer class="footer has-black-color-overlay has-dark-texture">
            <div class="container">
                <div class="row clearfix">
                    <?php dynamic_sidebar( 'footer-sidebar' ); ?>
                </div>
            </div>
    </footer><!-- /.footer -->
    <?php } ?>
    <?php if(!(landshaper_set($options, 'hide_bottom_footer'))):?>
	<section class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<?php if(landshaper_set($options, 'logo_image')):?>
						<img src="<?php echo esc_url(landshaper_set($options, 'logo_image'));?>" alt="" title="<?php esc_html_e('Awesome Image', 'landshaper');?>" class="center-block" >
	                <?php else:?>
						<img src="<?php echo esc_url(get_template_directory_uri().'/img/logo.png');?>" alt="<?php esc_html_e('Awesome Image', 'landshaper');?>" class="center-block" >
	                <?php endif;?>
					<p style="margin-top: 10px;">
						<strong>SL Agritech Corporation (SLAC)</strong> is a private company engaged in the research, <br>development, production and distribution of hybrid rice seed and premium quality rice.
					</p>
					<div>
						<a href="https://www.facebook.com/SLAgritech" class="social-fb"><i class="fa fa-2x fa-facebook-official"></i></a> &nbsp;&nbsp;
						<a href="https://www.youtube.com/channel/UCZTNNvvLI9TDsp27EZRQm5A/featured" class="social-fb"><i class="fa fa-2x fa-youtube"></i></a>
					</div>
				</div>
			</div>
		</div>
	</section>
    <section class="footer-bottom">
        <div class="container">
            <div class="pull-left copy-text">
                <p><?php echo wp_kses_post(landshaper_set($options, 'copyright'));?></p>
            </div><!-- /.pull-left -->
            <div class="pull-right get-text">
                <p><a href="<?php echo esc_url(landshaper_set($options, 'purchase_link'));?>"><?php echo balanceTags(landshaper_set($options, 'purchase'));?></a></p>
            </div><!-- /.pull-right -->
        </div><!-- /.container -->
    </section><!-- /.footer-bottom -->
    <?php endif;?>
    
    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>
	</div>
<?php wp_footer(); ?>
</body>
</html>