<form action="<?php echo esc_url(home_url('/')); ?>" class="clearfix">
    <input type="text" placeholder="<?php esc_html_e('Search Here', 'landshaper');?>" />
    <button type="submit"><i class="fa fa-search"></i></button>
</form>