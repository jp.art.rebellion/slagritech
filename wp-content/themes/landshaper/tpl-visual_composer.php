<?php /* Template Name: VC Page */
	get_header() ;
	$meta = _WSH()->get_meta('_bunch_header_settings');
	$bg = landshaper_set($meta, 'header_img');
	$title = landshaper_set($meta, 'header_title');
	$link = landshaper_set($meta, 'header_link');
?>
<?php if(landshaper_set($meta, 'breadcrumb')):?>
	
    <div class="inner-banner has-base-color-overlay text-center" <?php if($bg):?>style="background-image:url('<?php echo esc_attr($bg)?>');"<?php endif;?>>
        <div class="container">
            <div class="box">
                <h3><?php if($title) echo wp_kses_post($title); else wp_title('');?></h3>
            </div><!-- /.box -->
        </div><!-- /.container -->
    </div><!-- /.inner-banner -->
    
    <div class="breadcumb-wrapper">
        <div class="container">
            <div class="pull-left">
                <?php echo balanceTags(landshaper_get_the_breadcrumb()); ?>
            </div><!-- /.pull-left -->
        </div><!-- /.container -->
    </div><!-- /.breadcumb-wrapper -->
    
<?php endif;?>
<?php while( have_posts() ): the_post(); ?>
    <?php the_content(); ?>
<?php endwhile;  ?>
<?php get_footer() ; ?>