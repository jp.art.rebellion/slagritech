<?php


/** Set ABSPATH for execution */
define( 'ABSPATH', dirname(dirname(__FILE__)) . '/' );
define( 'WPINC', 'wp-includes' );


/**
 * @ignore
 */
function add_filter() {}

/**
 * @ignore
 */
function esc_attr($str) {return $str;}

/**
 * @ignore
 */
function apply_filters() {}

/**
 * @ignore
 */
function get_option() {}

/**
 * @ignore
 */
function is_lighttpd_before_150() {}

/**
 * @ignore
 */
function add_action() {}

/**
 * @ignore
 */
function did_action() {}

/**
 * @ignore
 */
function do_action_ref_array() {}

/**
 * @ignore
 */
function get_bloginfo() {}

/**
 * @ignore
 */
function is_admin() {return true;}

/**
 * @ignore
 */
function site_url() {}

/**
 * @ignore
 */
function admin_url() {}

/**
 * @ignore
 */
function home_url() {}

/**
 * @ignore
 */
function includes_url() {}

/**
 * @ignore
 */
function wp_guess_url() {}

if ( ! function_exists( 'json_encode' ) ) :
/**
 * @ignore
 */
function json_encode() {}
endif;



/* Convert hexdec color string to rgb(a) string */
 
function hex2rgba($color, $opacity = false) {
 
	$default = 'rgb(0,0,0)';
 
	//Return default if no color provided
	if(empty($color))
          return $default; 
 
	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}
$green = $_GET['main_color'];

ob_start(); ?>






ul.bulleted-list li:before,
.header .header-top .bootstrap-select .dropdown-menu > li > a:hover,
.header-navigation.navbar .right-box.nav > li > a .fa-search,
.header-4 .header-navigation.navbar .right-navigation .cart-button .count,
#slider1 .banner-decor-line,
.single-service-page .price-label,
.single-service-page .qoute-box:before,
.service-gallery-carousel .img-box .img-box-wrapper .overlay:before,
.single-news-post ul.meta-info li + li a:before,
.client-testimonial-carousel.owl-theme .owl-dots .owl-dot.active span,
.testimonial-style-two.with-carousel .owl-theme .owl-dots .owl-dot.active span,
.contact-form .form-result .inner,
.inner-banner,
.single-sidebar .sidebar-links ul li:hover a,
.single-sidebar .sidebar-links ul li.active a,
.single-sidebar .service-list-widget ul li.active a,
.single-sidebar .service-list-widget ul li:hover a,
.product-content .share-box li a:hover,
.product-tab-box .tab-title li.active a,
.product-tab-box .tab-title li a:hover,
.cart-section .thm-btn.update-cart:hover,
.accordion.style-three a[role=button],
.base-color-bg,
.thm-btn,
.thm-btn.inverse:hover,
.sec-title span.decor-line,
.sec-title-two .decor-line:before,
.has-base-color-overlay:before,
.post-navigation a:hover,
.header .header-top .bootstrap-select > .dropdown-toggle,
.header-navigation.navbar .right-box.nav > li > a .fa-search,
.header-navigation.navbar .right-box.nav > li > .sub-menu form button,
.rev_slider_wrapper #slider1 .banner-decor-line,
.single-service-box-two .icon-box i,
.single-service-box-two:hover,
.single-service-box-four .icon-box,
.single-service-box-four:hover,
.single-service-box-four:hover .icon-box i,
.service-gallery-carousel .owl-nav [class*=owl-],
.service-gallery-carousel .img-box .img-box-wrapper .overlay:before,
.single-project-item .img-box .overlay .top li a,
.single-project-item .img-box .overlay .bottom,
.related-project-carousel .owl-nav [class*=owl-],
.single-news-post ul.meta-info li + li a:before,
.footer-widget.link-widget li a:hover:before,
.footer-widget.subscribe-widget form button,
.footer-widget.contact-widget .footer-contact-info-carousel.owl-theme .owl-dots .owl-dot.active span,
.contact-form .bootstrap-select .dropdown-menu > li > a:hover,
.contact-section.contact-page .footer-widget.working-hrs-widget .working-hrs li span.labed,
.inner-banner,
.single-sidebar .download-broucher .text-box,
.single-sidebar .search-widget form button,
.single-sidebar .archive-widget ul li a:hover:before,
.single-sidebar .price-filter button,
.single-sidebar .product-category-widget ul li:hover,
.shop-page .meta-info .select-box .dropdown-menu > li > a:hover,
.single-shop-item .img-box .overlay .box .content ul li a,
.single-shop-item .text-box .thm-btn:hover,
.featured-product-carousel .owl-nav [class*=owl-],
.cart-table tbody .available-info .icon,
.cart-section .estimate-form .select-box .dropdown-menu > li > a:hover,
.accordion a[role="button"]:before,
.accordion.style-two a[role="button"]:before,
.about-tab-wrapper ul.tab-title-box li.active a span:after,
.about-tab-wrapper ul.tab-title-box li:hover a span:after,
.appoinment-section .contact-form .feature-list,
.add-banner,
.rfe-box .contact-form .button-box .wpcf7-submit,
.post-navigation li:hover a,
.post-navigation li span,
.header .header-navigation.navbar .right-box.nav > li > a .fa-search,
.header-navigation.navbar .right-box.nav > li > a .fa-search,
.footer-widget .subscribe-widget form button,
.woocommerce a.button,
.single-shop-item .img-box .overlay .box .content ul li i.fa-link,
.header .header-navigation.navbar .navbar-nav .sub-nav-toggler, .header-3 .navbar-nav .sub-nav-toggler
{

	background-color: #<?php echo $green; ?>;
}






.header-navigation.navbar .right-box.nav > li:hover > a,
.header .social a:hover,
.header-5 .contact-infos p,
.single-service-page a.know-more:hover,
.single-special-services i,
.service-gallery-carousel .img-box .img-box-wrapper .overlay .box .content i,
.single-sidebar .categories-widget ul li a:hover,
.cart-table tbody tr .sub-total,
.about-video .qouted-text,
.thm-btn.borderd,
.sec-title span.tag-line,
.scroll-to-top,
.header .header-top ul.contact-info-list > li .inner-box i,
.header-navigation.navbar .nav > li:hover > a,
.header-3 .nav > li:hover > a,
.header-3 .nav > li.current > a,
.header-navigation.navbar .navbar-nav > li > .sub-menu li:hover > a,
.header-navigation.navbar .right-box.nav > li:hover > a,
.single-service-box-two:hover .icon-box i,
.single-service-box-four .icon-box i,
.service-gallery-carousel .img-box .img-box-wrapper .overlay .box .content i,
.single-fact-box .counter,
.single-fact-box .symbol,
.single-fact-box:hover i,
.footer-widget.link-widget li a:hover,
.footer-widget.subscribe-widget p.highlight,
.footer-widget.subscribe-widget .result p,
.footer-widget.subscribe-widget .social li a:hover,
.footer-widget.contact-widget .contact-infos li .icon-box i,
.footer-bottom a,
.single-testimonial-home .text-box h3,
.single-testimonial-two .text-box:before,
.single-testimonial-two .name-box .title-box span,
.request-qoute-box p a,
.request-qoute-box p span.highlight,
.breadcumb-wrapper ul.link-list li span,
.breadcumb-wrapper a.get-qoute,
.single-sidebar .view-all,
.single-sidebar .latest-news .text-box h4:hover,
.single-sidebar .archive-widget ul li a:hover,
.single-sidebar .about-widget a,
.single-sidebar .best-seller .text-box h4:hover,
.single-sidebar .best-seller .text-box p,
.single-sidebar .agent-widget h4,
.single-sidebar .agent-widget ul li i,
.single-shop-item .text-box p,
.single-review-content .text-box h3 span,
.login-register a,
.checkout-section .payment-options .option-block .radio-block.active .icon i,
.checkout-section .payment-options .option-block .radio-block.active label.radio-label,
.single-price-box .name-box span,
.accordion a[role="button"].collapsed:before,
.accordion.style-two a[role="button"].collapsed:before,
.about-tab-wrapper ul.tab-title-box li.active a,
.about-tab-wrapper ul.tab-title-box li:hover a,
.base-color-text,
.navbar-default .navbar-nav>.active>a,
.navbar-default .navbar-nav>.active>a:focus,
.navbar-default .navbar-nav>.active>a:hover,
.header .header-navigation.navbar .nav > li:hover > a,
.header .header-navigation.navbar .right-box.nav > li:hover > a,
.footer-widget .subscribe-widget p.highlight,
.footer-widget .subscribe-widget .social li a:hover,
.header .header-navigation.navbar .navbar-nav > li > .sub-menu li:hover > a,
.breadcumb-wrapper ul.link-list li,
a:hover,
.single-sidebar.widget_categories ul > li:hover a,
.footer-widget .link-widget li a:hover,
.single-sidebar.widget_archive ul > li:hover a
{
	color: #<?php echo $green; ?>;
}





.thm-btn,
.thm-btn.inverse:hover,
.thm-btn.borderd,
.thm-btn.borderd.inverse:hover,
.scroll-to-top,
.header-navigation.navbar .navbar-nav > li > .sub-menu,
.header-navigation.navbar .navbar-nav > li > .sub-menu > li > .sub-menu,
.header-navigation.navbar .right-box.nav > li > .sub-menu form,
.header.header-2.header .header-navigation.navbar .right-box.nav li .thm-btn,
.header-3.header-navigation.navbar + section,
.header-3.header-navigation.navbar + div,
#slider1 .banner-caption-box,
.fixed-banner .banner-caption-box,
.single-service-box-one .img-box .overlay .box,
.single-service-page .qoute-box,
.service-gallery-carousel .owl-nav [class*=owl-],
.related-project-carousel .owl-nav [class*=owl-],
.single-fact-box,
.single-news-post .img-box .overlay .box,
.single-news-post:hover,
.single-news-post:hover .img-box .overlay .box,
.single-testimonial-home,
.single-testimonial-home:before,
.client-testimonial-carousel.owl-theme .owl-dots .owl-dot span,
.testimonial-style-two.with-carousel .owl-theme .owl-dots .owl-dot span,
.single-sidebar .sidebar-links ul li:hover a,
.single-sidebar .sidebar-links ul li.active a,
.single-sidebar .latest-news .icon-box .overlay,
.single-sidebar .categories-widget ul li a:hover:before,
.single-sidebar .best-seller .icon-box .overlay,
.single-shop-item .img-box .offer-box .inner:before,
.featured-product-carousel .owl-nav [class*=owl-],
.product-content .share-box li a:hover,
.product-tab-box .tab-title li.active a,
.product-tab-box .tab-title li a:hover,
.single-price-box .price-box:before,
.accordion a[role="button"]:before,
.accordion.style-two a[role="button"]:before,
.single-team-member .img-box .overlay .box,
.single-team-member:hover,
.single-team-member:hover .img-box .overlay .box,
.thm-btn,
.thm-btn.inverse:hover,
.thm-btn.borderd,
.thm-btn.borderd.inverse:hover,
.post-filter li.active span,
.post-navigation a:hover,
.scroll-to-top,
.header-navigation.navbar .navbar-nav > li > .sub-menu,
.header-navigation.navbar .navbar-nav > li > .sub-menu > li > .sub-menu,
.header-navigation.navbar .right-box.nav > li > .sub-menu form,
.rev_slider_wrapper #slider1 .banner-caption-box,
.fixed-banner .banner-caption-box,
.single-service-box-one .img-box .overlay .box,
.single-service-box-one:hover .img-box .overlay .box,
.single-service-box-four:hover .text-box:before,
.service-gallery-carousel .owl-nav [class*=owl-],
.related-project-carousel .owl-nav [class*=owl-],
.single-fact-box,
.single-news-post .img-box .overlay .box,
.single-news-post:hover,
.single-news-post:hover .img-box .overlay .box,
.footer-widget.link-widget li a:hover:before,
.footer-widget.contact-widget .footer-contact-info-carousel.owl-theme .owl-dots .owl-dot.active span,
.client-testimonial-carousel.owl-theme .owl-dots .owl-dot span,
.testimonial-style-two.with-carousel .owl-theme .owl-dots .owl-dot span,
.single-sidebar .latest-news .icon-box .overlay,
.single-sidebar .categories-widget ul li a:hover:before,
.single-sidebar .archive-widget ul li a:hover:before,
.single-sidebar .price-filter .noUi-connect,
.single-sidebar .price-filter .noUi-handle, 
.single-sidebar .best-seller .icon-box .overlay,
.single-shop-item .text-box .thm-btn:hover,
.single-shop-item:hover .img-box .overlay .box,
.featured-product-carousel .owl-nav [class*=owl-],
.cart-section .thm-btn.update-cart:hover,
.accordion a[role="button"]:before,
.accordion.style-two a[role="button"]:before, 
.single-team-member:hover,
.single-team-member:hover .img-box .overlay .box,
.about-tab-wrapper ul.tab-title-box li.active a span:after,
.about-tab-wrapper ul.tab-title-box li:hover a span:after,
.header-3.header-navigation.navbar + section,
.header-3.header-navigation.navbar + div,
.single-testimonial-home:before,
.single-shop-item .img-box .offer-box .inner:before,
.single-price-box .price-box:before,
.single-testimonial-home,
.single-shop-item .img-box .offer-box .inner:before,
.post-navigation li:hover a,
.post-navigation li span,
.thm-btn,
.thm-btn.inverse:hover,
.thm-btn.borderd,
.thm-btn.borderd.inverse:hover,
.single-service-box-two:hover::before,
.single-service-box-two::before,
.single-service-box-two:hover,
.header .header-navigation.navbar .navbar-nav > li > .sub-menu,
.single-sidebar.widget_categories ul > li:hover a::before,
.single-sidebar.widget_archive ul > li:hover a::before,
.woocommerce a.button,
.single-service-box-four:hover,
.header .header-navigation.navbar .navbar-nav .sub-nav-toggler, .header-3 .navbar-nav .sub-nav-toggler
{
	border-color: #<?php echo $green; ?>;
}



.single-sidebar .price-filter .noUi-connect,
.single-team-member:hover .img-box .overlay .box,
.single-news-post:hover .img-box .overlay .box,
.single-service-box-one:hover .img-box .overlay .box,
.navbar-default .navbar-toggle
{
	border-color: #<?php echo $green; ?> !important;
}



.single-service-page .single-service-box-two i,
.single-sidebar .price-filter .noUi-connect,
.single-sidebar .price-filter .noUi-connect,
.pagination > li > a:hover,
.pagination > li > span:hover,
.navbar-default .navbar-toggle
{
	background-color: #<?php echo $green; ?> !important;
	
}



.woocommerce a.button:hover{
   color: #<?php echo $green; ?> !important;

} 






<?php 

$out = ob_get_clean();
$expires_offset = 31536000; // 1 year
header('Content-Type: text/css; charset=UTF-8');
header('Expires: ' . gmdate( "D, d M Y H:i:s", time() + $expires_offset ) . ' GMT');
header("Cache-Control: public, max-age=$expires_offset");
header('Vary: Accept-Encoding'); // Handle proxies
header('Content-Encoding: gzip');

echo gzencode($out);
exit;