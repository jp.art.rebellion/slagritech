<?php $options = _WSH()->option();
	get_header(); 
	$settings  = landshaper_set(landshaper_set(get_post_meta(get_the_ID(), 'bunch_page_meta', true) , 'bunch_page_options') , 0);
	$meta = _WSH()->get_meta('_bunch_layout_settings');
	$meta1 = _WSH()->get_meta('_bunch_header_settings');
	$meta2 = _WSH()->get_meta();
	_WSH()->page_settings = $meta;
	if(landshaper_set($_GET, 'layout_style')) $layout = landshaper_set($_GET, 'layout_style'); else
	$layout = landshaper_set( $meta, 'layout', 'full' );
	if( !$layout || $layout == 'full' || landshaper_set($_GET, 'layout_style')=='full' ) $sidebar = ''; else
	$sidebar = landshaper_set( $meta, 'sidebar', 'blog-sidebar' );
	$classes = ( !$layout || $layout == 'full' || landshaper_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-lg-9 col-md-8 col-sm-12 col-xs-12 ' ;
	_WSH()->post_views( true );
	$bg = landshaper_set($meta1, 'header_img');
	$title = landshaper_set($meta1, 'header_title');
?>

<div class="inner-banner has-base-color-overlay text-center" <?php if($bg):?>style="background-image:url('<?php echo esc_attr($bg)?>');"<?php endif;?>>
    <div class="container">
        <div class="box">
            <h3><?php if($title) echo wp_kses_post($title); else wp_title('');?></h3>
        </div><!-- /.box -->
    </div><!-- /.container -->
</div><!-- /.inner-banner -->

<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <?php echo balanceTags(landshaper_get_the_breadcrumb()); ?>
        </div><!-- /.pull-left -->
    </div><!-- /.container -->
</div><!-- /.breadcumb-wrapper -->

<!--Sidebar Page-->
<section class="page sidebar-page sec-pad news-wrapper news-grid-sidebar-page">
    <div class="container">        
        <div class="row">
            
            <!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-md-3 col-sm-12 col-xs-12">        
				<?php dynamic_sidebar( $sidebar ); ?>
			</div>
			<?php } ?>
			<?php endif; ?>
            
            <!--Content Side-->	
            <div class="<?php echo esc_attr($classes);?>">
                
				<?php while( have_posts() ): the_post(); 
					$post_meta = _WSH()->get_meta();
				?>
                    
                <div class="single-news-post wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <?php if ( has_post_thumbnail() ):?>
                    <div class="img-box">
                        <?php the_post_thumbnail('landshaper_1170x500');?>                        
                    </div><!-- /.img-box -->
                    <?php endif;?>
                    <ul class="meta-info list-inline">
                        <li>
                            <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php esc_html_e('by', 'landshaper');?> <?php the_author();?></a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url(get_month_link(get_the_date('Y'), get_the_date('m'))); ?>"><?php echo get_the_date('M d, Y');?></a>
                        </li>
                        <li>
                            <?php the_category('', ', ', ''); ?>
                        </li>
                        <li>
                            <a href="<?php echo esc_url(get_permalink(get_the_id()).'#comment');?>"><?php comments_number( '0 comments', '1 comment', '% comments' ); ?></a>
                        </li>
                    </ul><!-- /.meta-info list-inline -->
                    
                    <?php the_content();?>
                    <?php the_tags('Tags: ', ', ');?>
                    <br>
                    <div class="clearfix"></div>
					<?php wp_link_pages(array('before'=>'<div class="paginate-links">'.esc_html__('Pages: ', 'landshaper'), 'after' => '</div>', 'link_before'=>'<span>', 'link_after'=>'</span>')); ?>
                </div><!-- /.single-news-post -->

                <div class="share-box">
                    <span class="title">
                        <?php esc_html_e('Did You Like This Post?', 'landshaper');?>
                    </span>
                    <div class="share-links">
                        <a href="http://www.facebook.com/sharer.php?u=<?php echo esc_url(get_permalink(get_the_id()));?>" class="facebook-share"><span class="fa fa-facebook-f"></span> <?php esc_html_e('facebook', 'landshaper');?></a>
                        <a href="https://twitter.com/share?url=<?php echo esc_url(get_permalink(get_the_id()));?>&text=<?php echo esc_attr($post_slug=$post->post_name); ?>" class="twitter-share"><span class="fa fa-twitter"></span> <?php esc_html_e('Twitter', 'landshaper');?></a>
                        <a href="https://plus.google.com/share?url=<?php echo esc_url(get_permalink(get_the_id()));?>" class="instagram-share"><span class="fa fa-google-plus"></span> <?php esc_html_e('Google Plus', 'landshaper');?></a>
                        <a href="https://pinterest.com/pin/create/bookmarklet/?url=<?php echo esc_url(get_permalink(get_the_id()));?>&description=<?php echo esc_attr($post_slug=$post->post_name); ?>" class="pinterest-share"><span class="fa fa-pinterest-p"></span> <?php esc_html_e('Pinterest', 'landshaper');?></a>
                    </div>
                </div><!-- /.share-box -->
				<div class="clearfix"></div>
                <!-- comment area -->
                <?php comments_template(); ?><!-- end comments -->    
                
				<?php endwhile;?>
                
            </div>
            <!--Content Side-->
            
            <!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-md-3 col-sm-12 col-xs-12">        
				<?php dynamic_sidebar( $sidebar ); ?>
			</div>
			<?php } ?>
			<?php endif; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>