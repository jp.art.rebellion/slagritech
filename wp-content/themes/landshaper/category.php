<?php landshaper_bunch_global_variable();
	$options = _WSH()->option();
	get_header(); 
	$meta = _WSH()->get_term_meta( '_bunch_category_settings' );
	_WSH()->page_settings = $meta; 
	if(landshaper_set($_GET, 'layout_style')) $layout = landshaper_set($_GET, 'layout_style'); else
	$layout = landshaper_set( $meta, 'layout', 'right' );
	$sidebar = landshaper_set( $meta, 'sidebar', 'blog-sidebar' );
	$classes = ( !$layout || $layout == 'full' || landshaper_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-md-9 col-sm-12 col-xs-12 ' ;
	$bg = landshaper_set($meta, 'header_img');
	$title = landshaper_set($meta, 'header_title');
	$link = landshaper_set($meta, 'header_link');
?>

<!--Page Title-->
<div class="inner-banner has-base-color-overlay text-center" <?php if($bg):?>style="background-image:url('<?php echo esc_attr($bg)?>');"<?php endif;?>>
    <div class="container">
        <div class="box">
            <h3><?php if($title) echo wp_kses_post($title); else wp_title('');?></h3>
        </div><!-- /.box -->
    </div><!-- /.container -->
</div><!-- /.inner-banner -->

<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <?php echo balanceTags(landshaper_get_the_breadcrumb()); ?>
        </div><!-- /.pull-left -->
    </div><!-- /.container -->
</div><!-- /.breadcumb-wrapper -->

<!--Sidebar Page-->
<section class="page sidebar-page sec-pad news-wrapper news-grid-sidebar-page">
    <div class="container">        
        <div class="row">
            
            <!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-md-3 col-sm-12 col-xs-12">        
				<?php dynamic_sidebar( $sidebar ); ?>
			</div>
			<?php } ?>
			<?php endif; ?>
            
            <!--Content Side-->	
            <div class="<?php echo esc_attr($classes);?>">
                <!--Blog Post-->
				<?php while( have_posts() ): the_post();?>
                    <!-- blog post item -->
                    <!-- Post -->
                    <div id="post-<?php the_ID(); ?>" <?php post_class();?>>
                        <?php get_template_part( 'blog' ); ?>
                    <!-- blog post item -->
                    </div><!-- End Post -->
                <?php endwhile;?>
                
                <!--Pagination-->
                <div class="post-nav-wrapper clearfix">
                    <div class="pull-left">
                        <div class="post-navigation list-inline ">
                            <?php landshaper_the_pagination(); ?>
                        </div><!-- /.post-navigation -->
                    </div><!-- /.pull-left -->
                </div><!-- /.post-nav-wrapper -->
            </div>
            <!--Content Side-->
            
            <!--Sidebar-->	
            <!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-md-3 col-sm-12 col-xs-12">        
				<?php dynamic_sidebar( $sidebar ); ?>
			</div>
			<?php } ?>
			<?php endif; ?>
            <!--Sidebar-->
        </div>
    </div>
</section>

<?php get_footer(); ?>