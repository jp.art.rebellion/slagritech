<form action="<?php echo esc_url(home_url('/'));?>" class="search-form">
    <input type="text" name="s" placeholder="<?php esc_html_e('Search...', 'landshaper');?>" />
    <button type="submit"><i class="fa fa-search"></i></button>
</form>