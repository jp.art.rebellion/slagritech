<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="product-content">
        <div class="row">
            
            <div class="col-md-5">
                <div class="product-thumb">
                    <div class="single-product-carousel-content-box">
                        <div class="item">
                            <div class="img-box">
                                <?php
									/**
									 * woocommerce_before_single_product_summary hook
									 *
									 * @hooked woocommerce_show_product_sale_flash - 10
									 * @hooked woocommerce_show_product_images - 20
									 */
									do_action( 'woocommerce_before_single_product_summary' );
								?>
                            </div>
                        </div>
                    </div>
            
                </div>
            </div>
	
            <div class="summary entry-summary">

				<?php
                    /**
                     * woocommerce_single_product_summary hook
                     *
                     * @hooked woocommerce_template_single_title - 5
                     * @hooked woocommerce_template_single_rating - 10
                     * @hooked woocommerce_template_single_price - 10
                     * @hooked woocommerce_template_single_excerpt - 20
                     * @hooked woocommerce_template_single_add_to_cart - 30
                     * @hooked woocommerce_template_single_meta - 40
                     * @hooked woocommerce_template_single_sharing - 50
                     */
                    do_action( 'woocommerce_single_product_summary' );
                ?>
        
                <div class="col-md-7">
                    <div class="title-box clearfix">
                        <?php woocommerce_template_single_title();?>
                    </div><!-- /.title-box -->

                    <span class="price h2 "><span class="small base-color-text secondary-font"><strong><?php woocommerce_template_single_price();?></strong></span></span>
                    <div class="review-box">
                        <a href="#" class="base-color-text"><?php woocommerce_template_single_rating();?></a>
                    </div><!-- /.review-box -->
                    <h3 class="secondary-font secondary-font c-black h4"><strong><?php esc_html_e('Product Overview:', 'landshaper');?></strong></h3>
                    <?php woocommerce_template_single_excerpt();?>
                    
                    <div class="cart-box">
                        <?php woocommerce_template_single_add_to_cart();?>
                    </div>

                </div>

            </div><!-- .summary -->

        </div>
    <?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php echo esc_url(get_permalink(get_the_id())); ?>" />
	</div>
</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
