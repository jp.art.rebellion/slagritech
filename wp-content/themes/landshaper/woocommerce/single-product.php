<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$meta = _WSH()->get_meta('_bunch_layout_settings');
$meta1 = _WSH()->get_meta('_bunch_header_settings');
$layout = landshaper_set( $meta, 'layout', 'right' );
$sidebar = landshaper_set( $meta, 'sidebar', 'blog-sidebar' );

$layout = ($layout) ? $layout : 'right';
$sidebar = ($sidebar) ? $sidebar : 'blog-sidebar';

$classes = ( !$layout || $layout == 'full' || landshaper_set($_GET, 'layout_style')=='full' ) ? ' col-md-12 col-sm-12 col-xs-12 ' : ' col-md-9 col-sm-12 col-xs-12 ' ;

$bg = landshaper_set($meta1, 'header_img');
$title = landshaper_set($meta1, 'header_title');
$link = landshaper_set($meta1, 'header_link');

get_header( 'shop' ); ?>

<!--Page Title-->
<div class="inner-banner has-base-color-overlay text-center" <?php if($bg):?>style="background-image:url('<?php echo esc_attr($bg)?>');"<?php endif;?>>
    <div class="container">
        <div class="box">
            <h3><?php if($title) echo balanceTags($title); else wp_title('');?></h3>
        </div><!-- /.box -->
    </div><!-- /.container -->
</div><!-- /.inner-banner -->

<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <?php echo balanceTags(landshaper_get_the_breadcrumb()); ?>
        </div><!-- /.pull-left -->
        <div class="pull-right">
            <a href="<?php if($link) echo balanceTags($link); ?>" class="get-qoute"><i class="fa fa-arrow-circle-right"></i> <?php esc_html_e('Make an appointment', 'landshaper');?></a>
        </div><!-- /.pull-right -->
    </div><!-- /.container -->
</div><!-- /.breadcumb-wrapper -->

<section class="sec-pad shop-sidebar shop-page">
	<div class="container">
        <div class="row">
			
            <!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-md-3 col-sm-12  col-xs-12">        
				<?php dynamic_sidebar( $sidebar ); ?>
				<?php
					/**
					 * woocommerce_sidebar hook
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					do_action( 'woocommerce_sidebar' );
				?>
			</div>
			<?php } ?>
			<?php endif; ?>
			
            <div class="<?php echo esc_attr($classes);?> ls-detail-page">
            <?php
                /**
                 * woocommerce_before_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 */
                do_action( 'woocommerce_before_main_content' );
				?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php wc_get_template_part( 'content', 'single-product' ); ?>
					<?php endwhile; // end of the loop. ?>
				<?php
                /**
                 * woocommerce_after_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                do_action( 'woocommerce_after_main_content' );
            ?>
            
            </div>
		
        <!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-md-3 col-sm-12  col-xs-12">        
				<?php dynamic_sidebar( $sidebar ); ?>
				<?php
                    /**
                     * woocommerce_sidebar hook
                     *
                     * @hooked woocommerce_get_sidebar - 10
                     */
                    do_action( 'woocommerce_sidebar' );
                ?>
			</div>
			<?php } ?>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php get_footer( 'shop' ); ?>
