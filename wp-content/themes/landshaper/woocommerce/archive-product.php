<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$meta = _WSH()->get_meta('_bunch_layout_settings', get_option( 'woocommerce_shop_page_id' ));
$meta1 = _WSH()->get_meta('_bunch_header_settings', get_option( 'woocommerce_shop_page_id' ));
$layout = landshaper_set( $meta, 'layout', 'right' );
$layout = landshaper_set( $_GET, 'layout' ) ? $_GET['layout'] : $layout; 
if(landshaper_set($_GET, 'layout_style')) $layout = landshaper_set($_GET, 'layout_style'); else
$layout = landshaper_set( $meta, 'layout', 'right' );
$sidebar = landshaper_set( $meta, 'sidebar', 'blog-sidebar' );

$layout = ($layout) ? $layout : 'right';
$sidebar = ($sidebar) ? $sidebar : 'blog-sidebar';

$classes = ( !$layout || $layout == 'full' || landshaper_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : 'col-md-9 col-sm-12 col-xs-12 ' ;
  
$bg = landshaper_set($meta1, 'header_img');
$title = landshaper_set($meta1, 'header_title');
$link = landshaper_set($meta1, 'header_link');

get_header( 'shop' ); ?>

<!--Page Title-->
<div class="inner-banner has-base-color-overlay text-center" <?php if($bg):?>style="background-image:url('<?php echo esc_attr($bg)?>');"<?php endif;?>>
    <div class="container">
        <div class="box">
            <h3><?php if($title) echo balanceTags($title); else wp_title('');?></h3>
        </div><!-- /.box -->
    </div><!-- /.container -->
</div><!-- /.inner-banner -->

<div class="breadcumb-wrapper">
    <div class="container">
        <div class="pull-left">
            <?php echo balanceTags(landshaper_get_the_breadcrumb()); ?>
        </div><!-- /.pull-left -->
        <div class="pull-right">
            <a href="<?php if($link) echo balanceTags($link); ?>" class="get-qoute"><i class="fa fa-arrow-circle-right"></i> <?php esc_html_e('Make an appointment', 'landshaper');?></a>
        </div><!-- /.pull-right -->
    </div><!-- /.container -->
</div><!-- /.breadcumb-wrapper -->

<section class="sec-pad shop-sidebar shop-page">
	<div class="container">
        <div class="row">
			<!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>
			<?php if ( is_active_sidebar( $sidebar ) ) { ?>
			<div class="col-md-3 col-sm-12  col-xs-12">        
				<?php dynamic_sidebar( $sidebar ); ?>
				<?php
                    /**
                     * woocommerce_sidebar hook
                     *
                     * @hooked woocommerce_get_sidebar - 10
                     */
                    do_action( 'woocommerce_sidebar' );
                ?>
			</div>
			<?php } ?>
			<?php endif; ?>

			<!-- sidebar area -->
			
			<div class="<?php echo esc_attr($classes);?> ls-shop">
                <div class="row meta-info">
                    <div class="col-md-12">
                        <div class="number-of-product pull-left"><?php woocommerce_result_count();?></div>
                        <div class="select-box pull-right">
                            <?php
								/**
								 * woocommerce_before_shop_loop hook
								 *
								 * @hooked woocommerce_result_count - 20
								 * @hooked woocommerce_catalog_ordering - 30
								 */
								do_action( 'woocommerce_before_shop_loop' );
							?>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
				<?php
                        /**
                         * woocommerce_before_main_content hook
                         *
                         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                         * @hooked woocommerce_breadcrumb - 20
                         */
                        do_action( 'woocommerce_before_main_content' );
                    ?>
                    
                    <?php
                        /**
                         * woocommerce_archive_description hook
                         *
                         * @hooked woocommerce_taxonomy_archive_description - 10
                         * @hooked woocommerce_product_archive_description - 10
                         */
                        do_action( 'woocommerce_archive_description' );
                    ?>
        
                <?php if ( have_posts() ) : ?>
        
                    <?php woocommerce_product_loop_start(); ?>
        
                        <?php woocommerce_product_subcategories(); ?>
        
                        <?php while ( have_posts() ) : the_post(); ?>
        
                            <?php wc_get_template_part( 'content', 'product' ); ?>
        
                        <?php endwhile; // end of the loop. ?>
        
                    <?php woocommerce_product_loop_end(); ?>
        
                    <?php
                        /**
                         * woocommerce_after_shop_loop hook
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action( 'woocommerce_after_shop_loop' );
                    ?>
        
                <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
        
                    <?php wc_get_template( 'loop/no-products-found.php' ); ?>
        
                <?php endif; ?>
        
            <?php
                /**
                 * woocommerce_after_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                do_action( 'woocommerce_after_main_content' );
            ?>
            </div>
    </div>

	<!-- sidebar area -->
	<?php if( $layout == 'right' ): ?>
    <?php if ( is_active_sidebar( $sidebar ) ) { ?>
    <div class="col-md-3 col-sm-12  col-xs-12">        
        <?php dynamic_sidebar( $sidebar ); ?>
		<?php
            /**
             * woocommerce_sidebar hook
             *
             * @hooked woocommerce_get_sidebar - 10
             */
            do_action( 'woocommerce_sidebar' );
        ?>
    </div>
    <?php } ?>
    <?php endif; ?>
    <!--Sidebar-->
    
		</div>
	</div>
</section>
<?php get_footer( 'shop' ); ?>
