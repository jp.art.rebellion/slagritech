<!--Blog Post-->
<div class="single-news-post wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
    <?php if ( has_post_thumbnail() ):?>
    <div class="img-box">
        <?php the_post_thumbnail('landshaper_1170x500');?>
        
        <div class="overlay">
            <div class="box">
                <div class="content">
                    <a href="<?php echo esc_url(get_permalink(get_the_id()));?>" class="thm-btn"><?php esc_html_e('Read More', 'landshaper');?></a>
                </div><!-- /.content -->
            </div><!-- /.box -->
        </div><!-- /.overlay -->
    </div><!-- /.img-box -->
    <?php endif;?>
    <ul class="meta-info list-inline">
        <li>
            <a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php esc_html_e('by', 'landshaper');?> <?php the_author();?></a>
        </li>
        <li>
            <a href="<?php echo esc_url(get_month_link(get_the_date('Y'), get_the_date('m'))); ?>"><?php echo get_the_date('M d, Y');?></a>
        </li>
        <li>
            <?php the_category('', ', ', ''); ?>
        </li>
        <li>
            <a href="<?php echo esc_url(get_permalink(get_the_id()).'#comment');?>"><?php comments_number( '0 comments', '1 comment', '% comments' ); ?></a>
        </li>
    </ul><!-- /.meta-info list-inline -->
    <h3><a href="<?php echo esc_url(get_permalink(get_the_id()));?>"><?php the_title();?></a></h3>
    <?php the_excerpt();?>
</div><!-- /.single-news-post -->

