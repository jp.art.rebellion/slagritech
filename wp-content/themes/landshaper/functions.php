<?php

//update_option( 'siteurl', 'http://localhost/slagritech' );
//update_option( 'home', 'http://localhost/slagritech' );

add_action('after_setup_theme', 'landshaper_bunch_theme_setup');
function landshaper_bunch_theme_setup()
{
	global $wp_version;
	if(!defined('LANDSHAPER_VERSION')) define('LANDSHAPER_VERSION', '1.0');
	if( !defined( 'LANDSHAPER_ROOT' ) ) define('LANDSHAPER_ROOT', get_template_directory().'/');
	if( !defined( 'LANDSHAPER_URL' ) ) define('LANDSHAPER_URL', get_template_directory_uri().'/');	
	include_once get_template_directory() . '/includes/loader.php';
	
	
	load_theme_textdomain('landshaper', get_template_directory() . '/languages');
	
	//ADD THUMBNAIL SUPPORT
	add_theme_support('post-thumbnails');
	add_theme_support('woocommerce');
	add_theme_support('menus'); //Add menu support
	add_theme_support('automatic-feed-links'); //Enables post and comment RSS feed links to head.
	add_theme_support('widgets'); //Add widgets and sidebar support
	add_theme_support( "title-tag" );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	/** Register wp_nav_menus */
	if(function_exists('register_nav_menu'))
	{
		register_nav_menus(
			array(
				/** Register Main Menu location header */
				'main_menu' => esc_html__('Main Menu', 'landshaper'),
			)
		);
	}
	if ( ! isset( $content_width ) ) $content_width = 960;
	add_image_size( 'landshaper_270x200', 270, 200, true ); // '270x200 Latest Project'
	add_image_size( 'landshaper_370x200', 370, 200, true ); // '370x200 Latest News'
	add_image_size( 'landshaper_80x80', 80, 80, true ); // '80x80 Testimonial Slider'
	add_image_size( 'landshaper_370x330', 370, 330, true ); // '370x330 Our Services'
	add_image_size( 'landshaper_60x60', 60, 60, true ); // '60x60 Testimonial Feedback'
	add_image_size( 'landshaper_270x240', 270, 240, true ); // '270x240 Our Team'
	add_image_size( 'landshaper_370x150', 370, 150, true ); // '370x150 Services Grid'
	add_image_size( 'landshaper_1250x543', 1250, 543, true ); // '1250x543 Gallery Masonry'
	add_image_size( 'landshaper_610x1149', 610, 1149, true ); // '610x1149 Gallery Masonry'
	add_image_size( 'landshaper_610x550', 610, 550, true ); // '610x550 Gallery Masonry'
	add_image_size( 'landshaper_270x170', 270, 170, true ); // '210x170 Blog List'
	add_image_size( 'landshaper_1170x500', 1170, 500, true ); // '1170x500 Our Blog'
	add_image_size( 'landshaper_75x75', 75, 75, true ); // '75x75 Our Recent Blog'
	add_image_size( 'landshaper_270x270', 270, 270, true ); // '270x270 Our Recent Blog'
	add_image_size( 'landshaper_67x64', 67, 64, true ); // '67x64 Our Recent Blog'
}
function landshaper_bunch_widget_init()
{
	global $wp_registered_sidebars;
	$theme_options = _WSH()->option();
	if( class_exists( 'Bunch_About_us' ) )register_widget( 'Bunch_About_us' );
	if( class_exists( 'Bunch_feed_burner' ) )register_widget( 'Bunch_feed_burner' );
	if( class_exists( 'Bunch_Contact_Info' ) )register_widget( 'Bunch_Contact_Info' );
	if( class_exists( 'Bunch_About_Info' ) )register_widget( 'Bunch_About_Info' );
	if( class_exists( 'Bunch_Recent_Post' ) )register_widget( 'Bunch_Recent_Post' );
	if( class_exists( 'Bunch_Services_Link' ) )register_widget( 'Bunch_Services_Link' );
	if( class_exists( 'Bunch_Agent_Info' ) )register_widget( 'Bunch_Agent_Info' );
	
	register_sidebar(array(
	  'name' => esc_html__( 'Default Sidebar', 'landshaper' ),
	  'id' => 'default-sidebar',
	  'description' => esc_html__( 'Widgets in this area will be shown on the right-hand side.', 'landshaper' ),
	  'before_widget'=>'<div id="%1$s" class="widget single-sidebar %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<div class="sec-title-two"><h3><span class="black-color-text">',
	  'after_title' => '</span></h3><span class="decor-line"></span></div>'
	));
	register_sidebar(array(
	  'name' => esc_html__( 'Footer Sidebar', 'landshaper' ),
	  'id' => 'footer-sidebar',
	  'description' => esc_html__( 'Widgets in this area will be shown in Footer Area.', 'landshaper' ),
	  'before_widget'=>'<div id="%1$s"  class="col-md-3 footer-widget %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<div class="sec-title-two"><h3><span class="white-color-text">',
	  'after_title' => '</span></h3><span class="decor-line"></span></div>'
	));
	
	register_sidebar(array(
	  'name' => esc_html__( 'Blog Listing', 'landshaper' ),
	  'id' => 'blog-sidebar',
	  'description' => esc_html__( 'Widgets in this area will be shown on the right-hand side.', 'landshaper' ),
	  'before_widget'=>'<div id="%1$s" class="widget single-sidebar %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<div class="sec-title-two"><h3><span class="black-color-text">',
	  'after_title' => '</span></h3><span class="decor-line"></span></div>'
	));
	if( !is_object( _WSH() )  )  return;
	$sidebars = landshaper_set(landshaper_set( $theme_options, 'dynamic_sidebar' ) , 'dynamic_sidebar' ); 
	foreach( array_filter((array)$sidebars) as $sidebar)
	{
		if(landshaper_set($sidebar , 'topcopy')) continue ;
		
		$name = landshaper_set( $sidebar, 'sidebar_name' );
		
		if( ! $name ) continue;
		$slug = landshaper_bunch_slug( $name ) ;
		
		register_sidebar( array(
			'name' => $name,
			'id' =>  $slug ,
			'before_widget' => '<div id="%1$s" class="side-bar widget single-sidebar %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h3>',
			'after_title' => '</h3>',
		) );		
	}
	
	update_option('wp_registered_sidebars' , $wp_registered_sidebars) ;
}
add_action( 'widgets_init', 'landshaper_bunch_widget_init' );
// Update items in cart via AJAX
add_filter('add_to_cart_fragments', 'landshaper_bunch_woo_add_to_cart_ajax');
function landshaper_bunch_woo_add_to_cart_ajax( $fragments ) {
    
	global $woocommerce;
    ob_start();
	
	get_template_part('includes/modules/wc_cart' );
	
	$fragments['li.wc-header-cart'] = ob_get_clean();
	
    return $fragments;
}
add_filter( 'woocommerce_enqueue_styles', '__return_false' );
function landshaper_load_head_scripts() {
    $options = _WSH()->option();
	if ( !is_admin() ) {
	$protocol = is_ssl() ? 'https://' : 'http://';
	$map_path = '?key='.landshaper_set($options, 'map_api_key');
	wp_enqueue_script( 'landshaper_map_api', $protocol.'maps.google.com/maps/api/js'.$map_path, array(), false, false );
	wp_enqueue_script( 'landshaper_jquery-googlemap', get_template_directory_uri().'/js/jquery.gmap.js', array(), false, false );
	wp_enqueue_script( 'html5shiv', get_template_directory_uri().'/js/html5shiv.js', array(), false, false );
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
	wp_enqueue_script( 'respond-min', get_template_directory_uri().'/js/respond.min.js', array(), false, false );
	wp_script_add_data( 'respond-min', 'conditional', 'lt IE 9' );
	}
    }
    add_action( 'wp_enqueue_scripts', 'landshaper_load_head_scripts' );
//global variables
function landshaper_bunch_global_variable() {
    global $wp_query;
}

function landshaper_enqueue_scripts() {
	$theme_options = _WSH()->option();
	$maincolor = str_replace( '#', '' , landshaper_set( $theme_options, 'main_color_scheme', '#A8C41B' ) );
    //styles
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap/bootstrap.min.css' );
	wp_enqueue_style( 'gui', get_template_directory_uri() . '/css/gui.css' );
	wp_enqueue_style( 'fontawesom', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'bootstrap-select', get_template_directory_uri() . '/css/bootstrap-select.min.css' );
	wp_enqueue_style( 'style-css', get_template_directory_uri() . '/css/lsp-icon/style.css' );
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css' );
	wp_enqueue_style( 'owl-theme', get_template_directory_uri() . '/css/owl.theme.default.min.css' );
	wp_enqueue_style( 'nouislider', get_template_directory_uri() . '/css/nouislider.css' );
	wp_enqueue_style( 'nouislider-pips', get_template_directory_uri() . '/css/nouislider.pips.css' );
	wp_enqueue_style( 'bootstrap-touchspin', get_template_directory_uri() . '/css/jquery.bootstrap-touchspin.css' );
	wp_enqueue_style( 'masterslider', get_template_directory_uri() . '/css/masterslider.css' );
	wp_enqueue_style( 'masterslider-style', get_template_directory_uri() . '/css/masterslider_style.css' );
	wp_enqueue_style( 'ms-staff-style', get_template_directory_uri() . '/css/ms-staff-style.css' );
	wp_enqueue_style( 'bxslider', get_template_directory_uri() . '/css/jquery.bxslider.min.css' );
	wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.min.css' );
	wp_enqueue_style( 'polyglot-language', get_template_directory_uri() . '/css/polyglot-language-switcher.css' );
	wp_enqueue_style( 'landshaper_main-style', get_stylesheet_uri() );
	wp_enqueue_style( 'customizer-style', get_template_directory_uri() . '/customizer/css/style.css' );
	wp_enqueue_style( 'color1', get_template_directory_uri() . '/css/skin/color1.css' );
	wp_enqueue_style( 'landshaper_responsive', get_template_directory_uri() . '/css/responsive.css' );
	if(class_exists('woocommerce')) wp_enqueue_style( 'landshaper_woocommerce', get_template_directory_uri() . '/css/woocommerce.css' );
	wp_enqueue_style( 'landshaper_custom-style', get_template_directory_uri() . '/css/custom.css' );
	wp_enqueue_style( 'landshaper-main-color', get_template_directory_uri() . '/css/color.php?main_color='.$maincolor );
	wp_enqueue_style( 'landshaper-color-panel', get_template_directory_uri() . '/css/color-panel.css' );
	
    //scripts
	wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'bootstrap-select', get_template_directory_uri().'/js/bootstrap-select.js', array('jquery'), false, true );
	wp_enqueue_script( 'gmaps', get_template_directory_uri().'/js/gmaps.js', array('jquery'), false, true );
	wp_enqueue_script( 'map-helper', get_template_directory_uri().'/js/map-helper.js', array('jquery'), false, true );
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri().'/js/owl.carousel.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'nouislider', get_template_directory_uri().'/js/nouislider.js', array('jquery'), false, true );
	wp_enqueue_script( 'bootstrap-touchspin', get_template_directory_uri().'/js/jquery.bootstrap-touchspin.js', array('jquery'), false, true );
	wp_enqueue_script( 'isotope', get_template_directory_uri().'/js/isotope.js', array('jquery'), false, true );
	wp_enqueue_script( 'masterslider', get_template_directory_uri().'/js/masterslider.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'bxslider', get_template_directory_uri().'/js/jquery.bxslider.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'Chart-min', get_template_directory_uri().'/js/Chart.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'circle-progress', get_template_directory_uri().'/js/circle-progress.js', array('jquery'), false, true );
	wp_enqueue_script( 'morris', get_template_directory_uri().'/js/morris.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'raphael-min', get_template_directory_uri().'/js/raphael.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'magnific-popup', get_template_directory_uri().'/js/jquery.magnific-popup.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'waypoints', get_template_directory_uri().'/js/waypoints.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'counterup-min', get_template_directory_uri().'/js/jquery.counterup.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'wow', get_template_directory_uri().'/js/wow.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'backstretch', get_template_directory_uri().'/js/jquery.backstretch.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'polyglot-language', get_template_directory_uri().'/js/jquery.polyglot.language.switcher.js', array('jquery'), false, true );
	wp_enqueue_script( 'jQuery-style-switcher', get_template_directory_uri().'/js/jQuery.style.switcher.min.js', array('jquery'), false, true );
	wp_enqueue_script( 'mCustomScrollbar', get_template_directory_uri().'/js/jquery.mCustomScrollbar.concat.min.js', array(), false, true );
	wp_enqueue_script( 'landshaper_main_script', get_template_directory_uri().'/js/custom.js', array(), false, true );
	if( is_singular() ) wp_enqueue_script('comment-reply');
	
}
add_action( 'wp_enqueue_scripts', 'landshaper_enqueue_scripts' );

/*-------------------------------------------------------------*/
function landshaper_theme_slug_fonts_url() {
    $fonts_url = '';
 
    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
    $lato = _x( 'on', 'Lato font: on or off', 'landshaper' );
	$roboto = _x( 'on', 'Roboto Slab font: on or off', 'landshaper' );
	$alegreya = _x( 'on', 'Alegreya font: on or off', 'landshaper' );
	$monteserat = _x( 'on', 'Monteserat font: on or off', 'landshaper' );
    /* Translators: If there are characters in your language that are not
    * supported by Open Sans, translate this to 'off'. Do not translate
    * into your own language.
    */
    $lato = _x( 'on', 'Lato font: on or off', 'landshaper' );
 
    if ( 'off' !== $lato || 'off' !== $roboto || 'off' !== $alegreya || 'off' !== $monteserat ) {
        $font_families = array();
		
		if ( 'off' !== $lato ) {
            $font_families[] = 'Lato';
        }
		
		if ( 'off' !== $roboto ) {
            $font_families[] = 'Roboto Slab:400,100,300,700';
        }
		
		if ( 'off' !== $monteserat ) {
            $font_families[] = 'Montserrat';
        }
		
		if ( 'off' !== $alegreya ) {
            $font_families[] = 'Alegreya:400,400italic,700italic,700,900,900italic';
        }
 
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
 
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }
 
    return esc_url_raw( $fonts_url );
}
function landshaper_theme_slug_scripts_styles() {
    wp_enqueue_style( 'landshaper-theme-slug-fonts', landshaper_theme_slug_fonts_url(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'landshaper_theme_slug_scripts_styles' );
/*---------------------------------------------------------------------*/
function landshaper_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'landshaper_add_editor_styles' );
/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */ 
function landshaper_woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 6;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'landshaper_jk_related_products_args' );
  function landshaper_jk_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}