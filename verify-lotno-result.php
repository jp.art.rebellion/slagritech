<?php include_once('config.php');

			//Connect to our MySQL database using the PDO extension.
$pdo = new PDO('mysql:host=localhost;dbname=i2797272_wp1', 'admin', 'myadminmail');
 
//Our select statement. This will retrieve the data that we want.
//$sql ="SELECT  lotno,region,variety FROM dalotinfo JOIN refregion ON dalotinfo.region = refregion.regDes ORDER BY refregion.id";

 $sql = "SELECT lotno FROM dalotinfo GROUP BY lotno ORDER BY id, region ASC  LIMIT 1"; 
 
//Prepare the select statement.
$stmt = $pdo->prepare($sql);
 
//Execute the statement.
$stmt->execute();
 
//Retrieve the rows using fetchAll.
$lotno = $stmt->fetchAll();

//foreach($lotno as $lotno): 
	
  //      echo $lotno['lotno'];
		
    //endforeach;
 
?>
<!doctype html>
<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml" xmlns:addthis="https://www.addthis.com/help/api-spec"  prefix="og: http://ogp.me/ns#" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>SLAC - LOT NUMBER VERIFICATION</title>
	
	<link rel="shortcut icon" href="https://demo.learncodeweb.com/favicon.ico">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="http://localhost:8889/phpcrud/css/mycss.css">
<script>

</script>

	
<script type="text/javascript">
</script>

</head>

<body class="bg2">
	

	<div class="bg-light border-bottom shadow-sm sticky-top">
		<div class="container">
			<header class="blog-header py-1">
				<nav class="navbar navbar-expand-lg navbar-light bg-light"> <a class="navbar-brand text-muted p-0 m-0" href="#"><img src='http://sl-agritech.com/wp-content/themes/landshaper/images/dalotverifyer.png'></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<!--	<ul class="navbar-nav mr-auto">
							<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-17" class="active nav-item"><a title="Home" href="https://learncodeweb.com/" class="nav-link">Home</a></li>
							<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-16" class="nav-item"><a title="Web Development" href="https://learncodeweb.com/learn/web-development/" class="nav-link">Web Development</a></li>
							<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-558" class="nav-item"><a title="PHP" href="https://learncodeweb.com/learn/php/" class="nav-link">PHP</a></li>
							<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-14" class="nav-item"><a title="Bootstrap" href="https://learncodeweb.com/learn/bootstrap-framework/" class="nav-link">Bootstrap</a></li>
							<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-559" class="nav-item"><a title="WordPress" href="https://learncodeweb.com/learn/wordpress/" class="nav-link">WordPress</a></li>
							<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-15" class="nav-item"><a title="Snippets" href="https://learncodeweb.com/learn/snippets/" class="nav-link">Snippets</a></li>
						</ul>
						
							-->
					</div>
				</nav>
			</header>
		</div> <!--/.container-->
	</div>
	
	<?php
    
	$condition	=	'';
	if(isset($_REQUEST['region']) and $_REQUEST['region']!=""){
		$condition	.=	' AND region LIKE "%'.$_REQUEST['region'].'%" ';
	}
	if(isset($_REQUEST['lotno']) and $_REQUEST['lotno']!=""){
		$condition	.=	' AND lotno LIKE "%'.$_REQUEST['lotno'].'%" ';
	}

	if(isset($_REQUEST['variety']) and $_REQUEST['variety']!=""){
		$condition	.=	' AND variety LIKE "%'.$_REQUEST['variety'].'%" ';
	}
	if(isset($_REQUEST['season']) and $_REQUEST['season']!=""){
		$condition	.=	' AND season LIKE "%'.$_REQUEST['season'].'%" ';
	}
	
	if(isset($_REQUEST['df']) and $_REQUEST['df']!=""){

		$condition	.=	' AND DATE(dt)>="'.$_REQUEST['df'].'" ';

	}
	if(isset($_REQUEST['dt']) and $_REQUEST['dt']!=""){

		$condition	.=	' AND DATE(dt)<="'.$_REQUEST['dt'].'" ';

	}
	
	//$sql ="SELECT  lotno,region,variety FROM dalotinfo JOIN refregion ON dalotinfo.region = refregion.regDes ORDER BY refregion.id";
	
$userData	=	$db->getAllRecords('i2797272_wp1.dalotinfo JOIN refregion ON dalotinfo.region = refregion.regDes','*',$condition,'ORDER BY refregion.id');
	
    //$userData1	=	$db->getAllRecords('test.dalotinfo','*',$condition,'GROUP BY lotno ASC');
	


	?>
	


   	<div class="container" style="opacity: 0.9;">
		<br>
		<!--<h1>SLAC DA Lot Number Records</a></h1> -->
		<div class="card">
		
			<div class="card-header"><i class="fas fa-book"></i> <strong>LOT NUMBER VERFICATION</strong></div>
			<div class="card-body">
				<?php
				if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rds"){
					echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record deleted successfully!</div>';
				}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rus"){
					echo	'<div class="alert alert-success"><i class="fa fa-thumbs-up"></i> Record updated successfully!</div>';
				}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rnu"){
					echo	'<div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> You did not change any thing!</div>';
				}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
					echo	'<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> There is some thing wrong <strong>Please try again!</strong></div>';
				}

				
				?>
				<div class="col-sm-12">
					<h5 class="card-title"><i class="fa fa-fw fa-search"></i> Lot Number Validation Results </h5>
					<form method="get">
						<div class="row">
							
							
							<div class="col-sm-3">
								<div class="form-group">
									<input type="text" name="lotno" id="lotno" class="form-control" value="<?php echo isset($_REQUEST['lotno'])?$_REQUEST['lotno']:''?>" placeholder="Enter Lot Number" maxlength="10" minlength="10" data-error="Minimum of 6 / Maximum of 16 characters" required>
								</div>

							</div>
							
							 
							 <div>
							
							
								<div class="form-group">
							<div>		
										<button type="submit" name="submit" value="search" id="submit" class="btn btn-primary"><i class="fa fa-fw fa-search"></i> Validate Another</button>
										<a href="verify-lotno-result.php" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Clear</a>
									
								</div>
							</div>
							
							
							
						</div>
					</form>
				</div>
			</div>
		</div>
		<hr>
		









		<!--<div>LOT NUMBER: 
		</div>-->
			
		<div id="newpost" >
			<table style="background-color:white;" class="table table-striped table-bordered">
				<thead>
					<tr class="bg-primary text-white">
						<!-- <th>RECID#</th> -->
						<th>Lot Number</th>
						<th>Region</th>
						<th>Variety</th>
						
						
						<!-- <th>No. Of Bag/s (15kg)</th> -->
						<!-- <th>Season</th> -->
						<!--<th class="text-center">Status</th>-->
					</tr>
				</thead>
				<tbody>
					<?php 
	                
				
				if(empty($_REQUEST['lotno'])){

    echo "<tr><td colspan='6' align='center'><i>LOT NUMBER input field must not be empty!</i></td></tr>";
return;
}
				
					if(count($userData)>0){
						$s	=	'';
						foreach($userData as $val){
							$s++;
							
					
  

					
					?>
					<tr>
						<!--<td><?php //echo $s;?></td>-->
						<td><?php echo $val['lotno'];?></td>
						<!-- <td><?php //echo substr($val['region'],2);?></td>-->
						<td><?php echo $val['region'];?></td>
						<td><?php echo $val['variety'];?></td>
						
						
						<?php
						
						//foreach($lotno as $lotno): 
	
	
    //echo $lotno['lotno']; 
    //endforeach;
	?>

						<!-- <td><?php //echo $val['season'];?></td>-->

						<!-- <td align="center">
						<?php //echo "<i>Valid Record</i>"; ?>
						<!--	<a href="edit-lotno.php?editId=<?php //echo $val['id'];?>" class="text-primary"><i class="fa fa-fw fa-edit"></i> Edit</a> | 
							 <a href="delete.php?delId=<?php //echo $val['id'];?>" class="text-danger" onClick="return confirm('Are you sure to delete this user?');"><i class="fa fa-fw fa-trash"></i> Delete</a>
						 </td> -->

					<!--</tr>-->
					<?php 
						}
						
						//echo "<tr><td colspan=0 align=top>xxxxxxx</td></tr>";
						
					}else{
					?>
					<tr><td colspan="6" align="center"><i>LOT NUMBER not found!</i></td></tr>
					<?php } ?>
				</tbody>
			</table>
		</div> <!--/.col-sm-12-->
		
	</div>
	
	
	</div>
	<div><center><p style="margin-top: 10px;">
						<strong>SL Agritech Corporation (SLAC)</strong> is a private company engaged in the research, <br>development, production and distribution of hybrid rice seed and premium quality rice.
					</p></center></div>
<script>

    function showhide()
     {
     var div = document.getElementById("newpost");
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
    else {
        div.style.display = "block";
    }
     }
    </script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/jquery.caret/0.1/jquery.caret.js"></script>
	<script src="https://www.solodev.com/_/assets/phone/jquery.mobilePhoneNumber.js"></script>
	<script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
  integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
  crossorigin="anonymous"></script>
    <script>
		$(document).ready(function() {
			jQuery(function($){
				  var input = $('[type=tel]')
				  input.mobilePhoneNumber({allowPhoneWithoutPrefix: '+1'});
				  input.bind('country.mobilePhoneNumber', function(e, country) {
					$('.country').text(country || '')
				  })
			 });
			 
			 //From, To date range start
			var dateFormat	=	"yy-mm-dd";
			fromDate	=	$(".fromDate").datepicker({
				changeMonth: true,
				dateFormat:'yy-mm-dd',
				numberOfMonths:2
			})
			.on("change", function(){
				toDate.datepicker("option", "minDate", getDate(this));
			}),
			toDate	=	$(".toDate").datepicker({
				changeMonth: true,
				dateFormat:'yy-mm-dd',
				numberOfMonths:2
			})
			.on("change", function() {
				fromDate.datepicker("option", "maxDate", getDate(this));
			});
			
			
			function getDate(element){
				var date;
				try{
					date = $.datepicker.parseDate(dateFormat,element.value);
				}catch(error){
					date = null;
				}
				return date;
			}
			//From, To date range End here	
			
		});
		
	</script>
</body>
</html>
